import os
import numpy as np
from shapely.geometry import Point as Point_shapely
from shapely.geometry.polygon import Polygon as Polygon_shapely
from helper_functions import generate_points_vtk, generate_faces_vtk
from helper_functions import calculate_barycenter, calculate_tria_area
from sympy import Point as Point_sympy
from sympy import Polygon as Polygon_sympy
from helper_functions import read_vtk, cells_area, split_quad_to_trias
from helper_functions import cells_area, generate_faces_vtk
from helper_functions import convert_node_data_to_cell_data

###############################################################################
#                       Load radiation and core boundary                      #
###############################################################################

# define path and vtk file of SOLPS core boundary
ggd_folder = "./all_ggd_vtks"
vtk_SOLPS_core_boundary = "shot123275_run1_CORE_BOUNDARY_radiation_dimension2.vtk"

# Define JINTRAC shots
shots = [134000]
runs = [60]

# Read SOLPS core boundary 
with open(os.path.join(ggd_folder, vtk_SOLPS_core_boundary)) as f:
    f.readline()
    f.readline()
    f.readline()
    f.readline()
    n_nodes_core = int(f.readline().split()[1])
    pts_core = []
    pts_core_polygon = []

    for node_id in range(n_nodes_core):
        points = [float(pt) for pt in f.readline().split()]
        pts_core.append(points)

    f.readline()
    n_cells_core = int(f.readline().split()[1])
    cells_core = []
    for cell_id_core in range(n_cells_core):
        cells_core.append([int(cell) for cell in f.readline().split()[1:]])

    f.readline()
    f.readline()
    for cell_id_core in range(n_cells_core):
        f.readline()


# Initialize lists and dictionaries to store data
radiation_profiles = {}
new_cells_q = {}
jintrac_points_shapely = []
files = []

for s in shots:
    for r in runs:
        file_name = "JINTRAC_radiation_shot"+str(s)+"_run"+str(r)+".vtk"
        data = read_vtk(file_name)

        for node_id in range(len(data["nodes"])):
            jintrac_points_shapely.append(Point_shapely(data["nodes"][node_id][0],
                                                        data["nodes"][node_id][2]))

        cells = np.array(data["cells"]).astype('int')
        cell_q = data["q_cells"]
        radiation_profiles["q_shot"+str(s)+"_run"+str(r)] = data["q_cells"]
        new_cells_q["q_shot"+str(s)+"_run"+str(r)] = []


pts_core_polygon_shapely = []
pts_core_polygon = []

for cell_core in enumerate(cells_core):
    iterator = cell_core[0]
    cell_core = cell_core[1]
    nodes = cell_core

    if iterator == 0:
        r0 = pts_core[nodes[0]][0]
        z0 = pts_core[nodes[0]][2]
        r1 = pts_core[nodes[1]][0]
        z1 = pts_core[nodes[1]][2]
        pts_core_polygon_shapely.append(Point_shapely(r0, z0))
        pts_core_polygon_shapely.append(Point_shapely(r1, z1))
        pts_core_polygon.append(Point_sympy(r0, z0))
        pts_core_polygon.append(Point_sympy(r1, z1))
    else:
        r1 = pts_core[nodes[1]][0]
        z1 = pts_core[nodes[1]][2]
        pts_core_polygon_shapely.append(Point_shapely(r1, z1))
        pts_core_polygon.append(Point_sympy(r1, z1))

core_bdry_shapely = Polygon_shapely(pts_core_polygon_shapely)
core_bdry = Polygon_sympy(*pts_core_polygon)

plot_points_inside_test_r = []
plot_points_inside_test_z = []

new_cells = []

plot_tria_1pt_inside_r = []
plot_tria_1pt_inside_z = []

points_id = [i for i in range(len(data["nodes"]))]
new_points_id = points_id[-1]
new_points = []
new_cell_q = []
new_cell_area = []
new_cell_center_r = []


for cell in enumerate(cells):
    cell_id = cell[0]
    cell = cell[1]
    pt0 = jintrac_points_shapely[cell[0]]
    pt1 = jintrac_points_shapely[cell[1]]
    pt2 = jintrac_points_shapely[cell[2]]
    pt3 = jintrac_points_shapely[cell[3]]

    points_within = []
    points_within_id = []

    if core_bdry_shapely.contains(pt0):
        points_within.append(pt0)
        points_within_id.append(cell[0])
    if core_bdry_shapely.contains(pt1):
        points_within.append(pt1)
        points_within_id.append(cell[1])
    if core_bdry_shapely.contains(pt2):
        points_within.append(pt2)
        points_within_id.append(cell[2])
    if core_bdry_shapely.contains(pt3):
        points_within.append(pt3)
        points_within_id.append(cell[3])

    if len(points_within) == 0:
        # JINTRAC cell is outside of SOLPS core boundary
        continue

    if len(points_within) == 4:
        # JINTRAC cell is fully inside of SOLPS core boundary
        plot_points_inside_test_r0 = [pt0.x]
        plot_points_inside_test_r0.append(pt1.x)
        plot_points_inside_test_r0.append(pt2.x)
        plot_points_inside_test_r0.append(pt0.x)
        plot_points_inside_test_z0 = [pt0.y]
        plot_points_inside_test_z0.append(pt1.y)
        plot_points_inside_test_z0.append(pt2.y)
        plot_points_inside_test_z0.append(pt0.y)
        plot_points_inside_test_r1 = [pt0.x]
        plot_points_inside_test_r1.append(pt2.x)
        plot_points_inside_test_r1.append(pt3.x)
        plot_points_inside_test_r1.append(pt0.x)
        plot_points_inside_test_z1 = [pt0.y]
        plot_points_inside_test_z1.append(pt2.y)
        plot_points_inside_test_z1.append(pt3.y)
        plot_points_inside_test_z1.append(pt0.y)

        cell1 = [cell[0], cell[1], cell[2]]
        cell2 = [cell[0], cell[2], cell[3]]
        new_cells.append(cell1)
        new_cells.append(cell2)
        new_cell_q.append(cell_q[cell_id])
        new_cell_q.append(cell_q[cell_id])
        for shot_run in radiation_profiles:
            new_cells_q[shot_run].append(radiation_profiles[shot_run][cell_id])
            new_cells_q[shot_run].append(radiation_profiles[shot_run][cell_id])

        new_cell_area.append(calculate_tria_area(plot_points_inside_test_r0[0],
                                                 plot_points_inside_test_z0[0],
                                                 plot_points_inside_test_r0[1],
                                                 plot_points_inside_test_z0[1],
                                                 plot_points_inside_test_r0[2],
                                                 plot_points_inside_test_z0[2]))
        new_cell_area.append(calculate_tria_area(plot_points_inside_test_r1[0],
                                                 plot_points_inside_test_z1[0],
                                                 plot_points_inside_test_r1[1],
                                                 plot_points_inside_test_z1[1],
                                                 plot_points_inside_test_r1[2],
                                                 plot_points_inside_test_z1[2]))
        new_cell_center_r.append(calculate_barycenter(plot_points_inside_test_r0[0],
                                                      plot_points_inside_test_z0[0],
                                                      plot_points_inside_test_r0[1],
                                                      plot_points_inside_test_z0[1],
                                                      plot_points_inside_test_r0[2],
                                                      plot_points_inside_test_z0[2])[0])
        new_cell_center_r.append(calculate_barycenter(plot_points_inside_test_r1[0],
                                                      plot_points_inside_test_z1[0],
                                                      plot_points_inside_test_r1[1],
                                                      plot_points_inside_test_z1[1],
                                                      plot_points_inside_test_r1[2],
                                                      plot_points_inside_test_z1[2])[0])

        continue


    # Check if core_boundary node is in rectangle
    quadrangle_shapely = Polygon_shapely([jintrac_points_shapely[cell[0]],
                                          jintrac_points_shapely[cell[1]],
                                          jintrac_points_shapely[cell[2]],
                                          jintrac_points_shapely[cell[3]]])
    core_point_inside = None

    for core_node in pts_core_polygon_shapely:
        if quadrangle_shapely.contains(core_node):
            core_point_inside = [core_node.x, core_node.y]

    # Get triangles if three nodes of cell is within SOLPS core boundary
    if len(points_within) == 3:
        points_within = [[float(points_within[0].x),
                          float(points_within[0].y)],
                         [float(points_within[1].x),
                          float(points_within[1].y)],
                         [float(points_within[2].x),
                          float(points_within[2].y)]]

        pt0_sympy = Point_sympy(pt0.x, pt0.y)
        pt1_sympy = Point_sympy(pt1.x, pt1.y)
        pt2_sympy = Point_sympy(pt2.x, pt2.y)
        pt3_sympy = Point_sympy(pt3.x, pt3.y)

        edge0 = Polygon_sympy(pt0_sympy, pt1_sympy)
        edge1 = Polygon_sympy(pt1_sympy, pt2_sympy)
        edge2 = Polygon_sympy(pt2_sympy, pt3_sympy)
        edge3 = Polygon_sympy(pt3_sympy, pt0_sympy)

        intersection_point0 = edge0.intersection(core_bdry)
        intersection_point1 = edge1.intersection(core_bdry)
        intersection_point2 = edge2.intersection(core_bdry)
        intersection_point3 = edge3.intersection(core_bdry)

        new_pentagon = []
        new_pentagon_id = []
        quadrangle_nodes_on_intersected_edges = []  # local IDs of nodes

        if len(intersection_point0) > 0:
            intersection_point0 = [float(intersection_point0[0].x), float(intersection_point0[0].y)]
            new_points.append([intersection_point0[0], intersection_point0[1], 0])
            if np.round(intersection_point0[0], decimals=5) == np.round(points_within[0][0], decimals=5) or \
               np.round(intersection_point0[1], decimals=5) == np.round(points_within[0][1], decimals=5):
                new_pentagon.append([intersection_point0, points_within[0]])
                new_points_id += 1
                new_pentagon_id.append([new_points_id, points_within_id[0]])
                quadrangle_nodes_on_intersected_edges.append(0)
            elif np.round(intersection_point0[0], decimals=5) == np.round(points_within[1][0], decimals=5) or \
               np.round(intersection_point0[1], decimals=5) == np.round(points_within[1][1], decimals=5):
                new_pentagon.append([intersection_point0, points_within[1]])
                new_points_id += 1
                new_pentagon_id.append([new_points_id, points_within_id[1]])
                quadrangle_nodes_on_intersected_edges.append(1)
            elif np.round(intersection_point0[0], decimals=5) == np.round(points_within[2][0], decimals=5) or \
               np.round(intersection_point0[1], decimals=5) == np.round(points_within[2][1], decimals=5):
                new_pentagon.append([intersection_point0, points_within[2]])
                new_points_id += 1
                new_pentagon_id.append([new_points_id, points_within_id[2]])
                quadrangle_nodes_on_intersected_edges.append(2)

        if len(intersection_point1) > 0:
            intersection_point1 = [float(intersection_point1[0].x), float(intersection_point1[0].y)]
            new_points.append([intersection_point1[0], intersection_point1[1], 0])
            if np.round(intersection_point1[0], decimals=5) == np.round(points_within[0][0], decimals=5) or \
               np.round(intersection_point1[1], decimals=5) == np.round(points_within[0][1], decimals=5):
                new_pentagon.append([intersection_point1, points_within[0]])
                new_points_id += 1
                new_pentagon_id.append([new_points_id, points_within_id[0]])
                quadrangle_nodes_on_intersected_edges.append(0)
            elif np.round(intersection_point1[0], decimals=5) == np.round(points_within[1][0], decimals=5) or \
               np.round(intersection_point1[1], decimals=5) == np.round(points_within[1][1], decimals=5):
                new_pentagon.append([intersection_point1, points_within[1]])
                new_points_id += 1
                new_pentagon_id.append([new_points_id, points_within_id[1]])
                quadrangle_nodes_on_intersected_edges.append(1)
            elif np.round(intersection_point1[0], decimals=5) == np.round(points_within[2][0], decimals=5) or \
               np.round(intersection_point1[1], decimals=5) == np.round(points_within[2][1], decimals=5):
                new_pentagon.append([intersection_point1, points_within[2]])
                new_points_id += 1
                new_pentagon_id.append([new_points_id, points_within_id[2]])
                quadrangle_nodes_on_intersected_edges.append(2)

        if len(intersection_point2) > 0:
            intersection_point2 = [float(intersection_point2[0].x), float(intersection_point2[0].y)]
            new_points.append([intersection_point2[0], intersection_point2[1], 0])
            if np.round(intersection_point2[0], decimals=5) == np.round(points_within[0][0], decimals=5) or \
               np.round(intersection_point2[1], decimals=5) == np.round(points_within[0][1], decimals=5):
                new_pentagon.append([intersection_point2, points_within[0]])
                new_points_id += 1
                new_pentagon_id.append([new_points_id, points_within_id[0]])
                quadrangle_nodes_on_intersected_edges.append(0)
            elif np.round(intersection_point2[0], decimals=5) == np.round(points_within[1][0], decimals=5) or \
                 np.round(intersection_point2[1], decimals=5) == np.round(points_within[1][1], decimals=5):
                new_pentagon.append([intersection_point2, points_within[1]])
                new_points_id += 1
                new_pentagon_id.append([new_points_id, points_within_id[1]])
                quadrangle_nodes_on_intersected_edges.append(1)
            elif np.round(intersection_point2[0], decimals=5) == np.round(points_within[2][0], decimals=5) or \
                 np.round(intersection_point2[1], decimals=5) == np.round(points_within[2][1], decimals=5):
                new_pentagon.append([intersection_point2, points_within[2]])
                new_points_id += 1
                new_pentagon_id.append([new_points_id, points_within_id[2]])
                quadrangle_nodes_on_intersected_edges.append(2)

        if len(intersection_point3) > 0:
            intersection_point3 = [float(intersection_point3[0].x), float(intersection_point3[0].y)]
            new_points.append([intersection_point3[0], intersection_point3[1], 0])
            if np.round(intersection_point3[0], decimals=5) == np.round(points_within[0][0], decimals=5) or \
               np.round(intersection_point3[1], decimals=5) == np.round(points_within[0][1], decimals=5):
                new_pentagon.append([intersection_point3, points_within[0]])
                new_points_id += 1
                new_pentagon_id.append([new_points_id, points_within_id[0]])
                quadrangle_nodes_on_intersected_edges.append(0)
            elif np.round(intersection_point3[0], decimals=5) == np.round(points_within[1][0], decimals=5) or \
               np.round(intersection_point3[1], decimals=5) == np.round(points_within[1][1], decimals=5):
                new_pentagon.append([intersection_point3, points_within[1]])
                new_points_id += 1
                new_pentagon_id.append([new_points_id, points_within_id[1]])
                quadrangle_nodes_on_intersected_edges.append(1)
            elif np.round(intersection_point3[0], decimals=5) == np.round(points_within[2][0], decimals=5) or \
               np.round(intersection_point3[1], decimals=5) == np.round(points_within[2][1], decimals=5):
                new_pentagon.append([intersection_point3, points_within[2]])
                new_points_id += 1
                new_pentagon_id.append([new_points_id, points_within_id[2]])
                quadrangle_nodes_on_intersected_edges.append(2)

        node_not_in_intersected_edges = 0
        for i in range(3):
            if i not in quadrangle_nodes_on_intersected_edges:
                node_not_in_intersected_edges = i

        new_pentagon = [new_pentagon[0][0], new_pentagon[0][1],
                        points_within[node_not_in_intersected_edges],
                        new_pentagon[1][1], new_pentagon[1][0]]
        new_pentagon_ids = [new_pentagon_id[0][0], new_pentagon_id[0][1],
                            points_within_id[node_not_in_intersected_edges],
                            new_pentagon_id[1][1], new_pentagon_id[1][0]]

        new_cells.append([new_pentagon_ids[0], new_pentagon_ids[1], new_pentagon_ids[2]])
        new_cells.append([new_pentagon_ids[2], new_pentagon_ids[0], new_pentagon_ids[4]])
        new_cells.append([new_pentagon_ids[2], new_pentagon_ids[3], new_pentagon_ids[4]])
        new_cell_q.append(cell_q[cell_id])
        new_cell_q.append(cell_q[cell_id])
        new_cell_q.append(cell_q[cell_id])
        for shot_run in radiation_profiles:
            new_cells_q[shot_run].append(radiation_profiles[shot_run][cell_id])
            new_cells_q[shot_run].append(radiation_profiles[shot_run][cell_id])
            new_cells_q[shot_run].append(radiation_profiles[shot_run][cell_id])

        new_cell_area.append(calculate_tria_area(new_pentagon[0][0],
                                                 new_pentagon[0][1],
                                                 new_pentagon[1][0],
                                                 new_pentagon[1][1],
                                                 new_pentagon[2][0],
                                                 new_pentagon[2][1]))
        new_cell_area.append(calculate_tria_area(new_pentagon[2][0],
                                                 new_pentagon[2][1],
                                                 new_pentagon[0][0],
                                                 new_pentagon[0][1],
                                                 new_pentagon[4][0],
                                                 new_pentagon[4][1]))
        new_cell_area.append(calculate_tria_area(new_pentagon[2][0],
                                                 new_pentagon[2][1],
                                                 new_pentagon[3][0],
                                                 new_pentagon[3][1],
                                                 new_pentagon[4][0],
                                                 new_pentagon[4][1]))
        new_cell_center_r.append(calculate_barycenter(new_pentagon[0][0],
                                                      new_pentagon[0][1],
                                                      new_pentagon[1][0],
                                                      new_pentagon[1][1],
                                                      new_pentagon[2][0],
                                                      new_pentagon[2][1])[0])
        new_cell_center_r.append(calculate_barycenter(new_pentagon[2][0],
                                                      new_pentagon[2][1],
                                                      new_pentagon[0][0],
                                                      new_pentagon[0][1],
                                                      new_pentagon[4][0],
                                                      new_pentagon[4][1])[0])
        new_cell_center_r.append(calculate_barycenter(new_pentagon[2][0],
                                                      new_pentagon[2][1],
                                                      new_pentagon[3][0],
                                                      new_pentagon[3][1],
                                                      new_pentagon[4][0],
                                                      new_pentagon[4][1])[0])

        if core_point_inside:
            new_points.append([core_point_inside[0], core_point_inside[1], 0])
            new_points_id += 1
            new_cells.append([new_pentagon_ids[0], new_points_id, new_pentagon_ids[4]])
            new_cell_q.append(cell_q[cell_id])
            for shot_run in radiation_profiles:
                new_cells_q[shot_run].append(radiation_profiles[shot_run][cell_id])

            new_cell_area.append(calculate_tria_area(new_pentagon[0][0],
                                                     new_pentagon[0][1],
                                                     core_point_inside[0],
                                                     core_point_inside[1],
                                                     new_pentagon[4][0],
                                                     new_pentagon[4][1]))
            new_cell_center_r.append(calculate_barycenter(new_pentagon[0][0],
                                                          new_pentagon[0][1],
                                                          core_point_inside[0],
                                                          core_point_inside[1],
                                                          new_pentagon[4][0],
                                                          new_pentagon[4][1])[0])

    # Get triangles if two nodes of cell are within SOLPS core boundary
    if len(points_within) == 2:
        points_within = [[float(points_within[0].x), float(points_within[0].y)],
                         [float(points_within[1].x), float(points_within[1].y)]]
        
        pt0_sympy = Point_sympy(pt0.x, pt0.y)
        pt1_sympy = Point_sympy(pt1.x, pt1.y)
        pt2_sympy = Point_sympy(pt2.x, pt2.y)
        pt3_sympy = Point_sympy(pt3.x, pt3.y)

        edge0 = Polygon_sympy(pt0_sympy, pt1_sympy)
        edge1 = Polygon_sympy(pt1_sympy, pt2_sympy)
        edge2 = Polygon_sympy(pt2_sympy, pt3_sympy)
        edge3 = Polygon_sympy(pt3_sympy, pt0_sympy)

        intersection_point0 = edge0.intersection(core_bdry)
        intersection_point1 = edge1.intersection(core_bdry)
        intersection_point2 = edge2.intersection(core_bdry)
        intersection_point3 = edge3.intersection(core_bdry)

        new_quadrangle = [[points_within[0][0], points_within[0][1]],
                          [points_within[1][0], points_within[1][1]]]
        new_quadrangle_ids = [points_within_id[0], points_within_id[1]]
        intersection_points = []
        if len(intersection_point0) > 0:
            intersection_point0 = [float(intersection_point0[0].x), float(intersection_point0[0].y)]
            intersection_points.append(intersection_point0)
            new_points.append([intersection_point0[0], intersection_point0[1], 0])
            if np.round(float(intersection_point0[0]), decimals=5) == np.round(points_within[0][0], decimals=5) or \
               np.round(float(intersection_point0[1]), decimals=5) == np.round(points_within[0][1], decimals=5):
                new_quadrangle.insert(0, [float(intersection_point0[0]), float(intersection_point0[1])])
                new_points_id += 1
                new_quadrangle_ids.insert(0, new_points_id)
            elif np.round(float(intersection_point0[0]), decimals=5) == np.round(points_within[1][0], decimals=5) or \
                 np.round(float(intersection_point0[1]), decimals=5) == np.round(points_within[1][1], decimals=5):
                new_quadrangle.append([float(intersection_point0[0], intersection_point0[1])])
                new_points_id += 1
                new_quadrangle_ids.append(new_points_id)

        if len(intersection_point1) > 0:
            intersection_point1 = [float(intersection_point1[0].x), float(intersection_point1[0].y)]
            new_points.append([intersection_point1[0], intersection_point1[1], 0])
            intersection_points.append(intersection_point1)
            #intersection_points.append(intersection_point1[0])
            if np.round(float(intersection_point1[0]), decimals=5) == np.round(points_within[0][0], decimals=5) or \
               np.round(float(intersection_point1[1]), decimals=5) == np.round(points_within[0][1], decimals=5):
                new_quadrangle.insert(0, [intersection_point1[0], intersection_point1[1]])
                new_points_id += 1
                new_quadrangle_ids.insert(0, new_points_id)

            elif np.round(float(intersection_point1[0]), decimals=5) == np.round(points_within[1][0], decimals=5) or \
                 np.round(float(intersection_point1[1]), decimals=5) == np.round(points_within[1][1], decimals=5):
                new_quadrangle.append([intersection_point1[0], intersection_point1[1]])
                new_points_id += 1
                new_quadrangle_ids.append(new_points_id)

        if len(intersection_point2) > 0:
            intersection_point2 = [float(intersection_point2[0].x), float(intersection_point2[0].y)]
            intersection_points.append(intersection_point2)
            new_points.append([intersection_point2[0], intersection_point2[1], 0])
            if np.round(float(intersection_point2[0]), decimals=5) == np.round(points_within[0][0], decimals=5) or \
               np.round(float(intersection_point2[1]), decimals=5) == np.round(points_within[0][1], decimals=5):
                new_quadrangle.insert(0, [intersection_point2[0], intersection_point2[1]])
                new_points_id += 1
                new_quadrangle_ids.insert(0, new_points_id)

            elif np.round(float(intersection_point2[0]), decimals=5) == np.round(points_within[1][0], decimals=5) or \
                 np.round(float(intersection_point2[1]), decimals=5) == np.round(points_within[1][1], decimals=5):
                new_quadrangle.append([intersection_point2[0], intersection_point2[1]])
                new_points_id += 1
                new_quadrangle_ids.append(new_points_id)

        if len(intersection_point3) > 0:
            intersection_points.append(intersection_point3)
            intersection_point3 = [float(intersection_point3[0].x), float(intersection_point3[0].y)]
            new_points.append([intersection_point3[0], intersection_point3[1], 0])
            if np.round(float(intersection_point3[0]), decimals=5) == np.round(points_within[0][0], decimals=5) or \
               np.round(float(intersection_point3[1]), decimals=5) == np.round(points_within[0][1], decimals=5):
                new_quadrangle.insert(0, [intersection_point3[0], intersection_point3[1]])
                new_points_id += 1
                new_quadrangle_ids.insert(0, new_points_id)

            elif np.round(float(intersection_point3[0]), decimals=5) == np.round(points_within[1][0], decimals=5) or \
                 np.round(float(intersection_point3[1]), decimals=5) == np.round(points_within[1][1], decimals=5):
                new_quadrangle.append([intersection_point3[0], intersection_point3[1]])
                new_points_id += 1
                new_quadrangle_ids.append(new_points_id)

        [trias, trias_ids] = split_quad_to_trias(new_quadrangle,
                                                 new_quadrangle_ids)
        tria0 = trias[0]
        tria1 = trias[1]

        new_cells.append([trias_ids[0][0], trias_ids[0][1], trias_ids[0][2]])
        new_cells.append([trias_ids[1][0], trias_ids[1][1], trias_ids[1][2]])
        new_cell_q.append(cell_q[cell_id])
        new_cell_q.append(cell_q[cell_id])
        for shot_run in radiation_profiles:
            new_cells_q[shot_run].append(radiation_profiles[shot_run][cell_id])
            new_cells_q[shot_run].append(radiation_profiles[shot_run][cell_id])

        new_cell_area.append(calculate_tria_area(tria0[0][0],
                                                 tria0[0][1],
                                                 tria0[1][0],
                                                 tria0[1][1],
                                                 tria0[2][0],
                                                 tria0[2][1]))
        new_cell_area.append(calculate_tria_area(tria1[0][0],
                                                 tria1[0][1],
                                                 tria1[1][0],
                                                 tria1[1][1],
                                                 tria1[2][0],
                                                 tria1[2][1]))
        new_cell_center_r.append(calculate_barycenter(tria0[0][0],
                                                      tria0[0][1],
                                                      tria0[1][0],
                                                      tria0[1][1],
                                                      tria0[2][0],
                                                      tria0[2][1])[0])
        new_cell_center_r.append(calculate_barycenter(tria1[0][0],
                                                      tria1[0][1],
                                                      tria1[1][0],
                                                      tria1[1][1],
                                                      tria1[2][0],
                                                      tria1[2][1])[0])

        if core_point_inside:
            new_points.append([core_point_inside[0], core_point_inside[1], 0])
            new_points_id += 1
            new_cells.append([new_quadrangle_ids[0], new_points_id, new_quadrangle_ids[-1]])
            new_cell_q.append(cell_q[cell_id])
            for shot_run in radiation_profiles:
                new_cells_q[shot_run].append(radiation_profiles[shot_run][cell_id])

            new_cell_area.append(calculate_tria_area(new_quadrangle[0][0],
                                                     new_quadrangle[0][1],
                                                     core_point_inside[0],
                                                     core_point_inside[1],
                                                     new_quadrangle[-1][0],
                                                     new_quadrangle[-1][1]))
            new_cell_center_r.append(calculate_barycenter(new_quadrangle[0][0],
                                                          new_quadrangle[0][1],
                                                          core_point_inside[0],
                                                          core_point_inside[1],
                                                          new_quadrangle[-1][0],
                                                          new_quadrangle[-1][1])[0])


    # Get triangles if just one node of cell is within SOLPS core boundary
    if len(points_within) == 1:
        pt0_sympy = Point_sympy(pt0.x, pt0.y)
        pt1_sympy = Point_sympy(pt1.x, pt1.y)
        pt2_sympy = Point_sympy(pt2.x, pt2.y)
        pt3_sympy = Point_sympy(pt3.x, pt3.y)

        edge0 = Polygon_sympy(pt0_sympy, pt1_sympy)
        edge1 = Polygon_sympy(pt1_sympy, pt2_sympy)
        edge2 = Polygon_sympy(pt2_sympy, pt3_sympy)
        edge3 = Polygon_sympy(pt3_sympy, pt0_sympy)

        intersection_point0 = edge0.intersection(core_bdry)
        intersection_point1 = edge1.intersection(core_bdry)
        intersection_point2 = edge2.intersection(core_bdry)
        intersection_point3 = edge3.intersection(core_bdry)

        new_triangle = [points_within[0]]
        new_triangle_id = [points_within_id[0]]
        plot_tria_1pt_inside_r = [points_within[0].x]
        plot_tria_1pt_inside_z = [points_within[0].y]
        intersection_points = []
        for intersection in [intersection_point0,
                             intersection_point1,
                             intersection_point2,
                             intersection_point3]:
            if len(intersection) > 0:
                new_points.append([float(intersection[0].x), float(intersection[0].y), 0])
                intersection_points.append([float(intersection[0].x), float(intersection[0].y), 0])
                new_points_id += 1
                new_triangle.append(intersection[0])
                new_triangle_id.append(new_points_id)
                plot_tria_1pt_inside_r.append(intersection[0].x)
                plot_tria_1pt_inside_z.append(intersection[0].y)

        new_cells.append(new_triangle_id)
        new_cell_q.append(cell_q[cell_id])
        for shot_run in radiation_profiles:
            new_cells_q[shot_run].append(radiation_profiles[shot_run][cell_id])

        new_cell_area.append(calculate_tria_area(plot_tria_1pt_inside_r[0],
                                                 plot_tria_1pt_inside_z[0],
                                                 plot_tria_1pt_inside_r[1],
                                                 plot_tria_1pt_inside_z[1],
                                                 plot_tria_1pt_inside_r[2],
                                                 plot_tria_1pt_inside_z[2]))
        new_cell_center_r.append(calculate_barycenter(plot_tria_1pt_inside_r[0],
                                                          plot_tria_1pt_inside_z[0],
                                                          plot_tria_1pt_inside_r[1],
                                                          plot_tria_1pt_inside_z[1],
                                                          plot_tria_1pt_inside_r[2],
                                                          plot_tria_1pt_inside_z[2])[0])

        if core_point_inside:
            new_points.append([core_point_inside[0], core_point_inside[1], 0])
            new_points_id += 1
            new_cells.append([new_triangle_id[1], new_points_id, new_triangle_id[2]])
            new_cell_q.append(cell_q[cell_id])
            for shot_run in radiation_profiles:
                new_cells_q[shot_run].append(radiation_profiles[shot_run][cell_id])

            new_cell_area.append(calculate_tria_area(core_point_inside[0],
                                                     core_point_inside[1],
                                                     plot_tria_1pt_inside_r[1],
                                                     plot_tria_1pt_inside_z[1],
                                                     plot_tria_1pt_inside_r[2],
                                                     plot_tria_1pt_inside_z[2]))
            new_cell_center_r.append(calculate_barycenter(core_point_inside[0],
                                                              core_point_inside[1],
                                                              plot_tria_1pt_inside_r[1],
                                                              plot_tria_1pt_inside_z[1],
                                                              plot_tria_1pt_inside_r[2],
                                                              plot_tria_1pt_inside_z[2])[0])


total_power = np.sum(np.array(new_cell_q)*np.array(new_cell_area)*np.array(new_cell_center_r)*2*np.pi)
print("Test_total_power:"+" "+str(total_power/1e6)+" MW")
for shot_run in radiation_profiles:
    total_power = np.sum(np.array(new_cells_q[shot_run])*np.array(new_cell_area)*np.array(new_cell_center_r)*2*np.pi)
    print("JINTRAC core "+shot_run+" "+str(total_power/1e6)+" MW")

name = "$q[W/m^3]$"+str(total_power/1e6)+"_MW"

q = {name: new_cell_q,
     "area[$m^2$]": new_cell_area,
     "cell center r[$m$]": new_cell_center_r}

r = np.array(new_points)[:, 0]
z = np.array(new_points)[:, 1]

new_points = np.array([r, np.zeros(len(r)), z]).T
points = np.concatenate((np.array(data["nodes"]), np.array(new_points)))
data_cells = {"nodes": points, "cells": new_cells}
data_cells.update(q)

generate_faces_vtk("JINTRAC_core_mesh_s134000_r60.vtk", data_cells)
