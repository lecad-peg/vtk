from helper_functions import read_vtk, generate_faces_vtk
from helper_functions import convert_quaddata2triadata, remove_orphan_nodes
from helper_functions import merge_two_triangular_meshes, remove_double_nodes, cell_centroid, cells_area
import numpy as np
import matplotlib.pyplot as plt

# Define SOLPS
shots = [123275, 123276, 123277, 123278, 123279]
data_solps_123275 = read_vtk("./all_ggd_vtks/shot123275_run1_Cells_radiation_dimension3.vtk")
data_solps_123276 = read_vtk("./all_ggd_vtks/shot123276_run1_Cells_radiation_dimension3.vtk")
data_solps_123277 = read_vtk("./all_ggd_vtks/shot123277_run1_Cells_radiation_dimension3.vtk")
data_solps_123278 = read_vtk("./all_ggd_vtks/shot123278_run1_Cells_radiation_dimension3.vtk")
data_solps_123279 = read_vtk("./all_ggd_vtks/shot123279_run1_Cells_radiation_dimension3.vtk")
data_solps = [data_solps_123275, data_solps_123276, data_solps_123277,
              data_solps_123278, data_solps_123279]

# Reorder and remove orphan nodes in JINTRAC
data_core_r60 = read_vtk("./JINTRAC_core_mesh_s134000_r60.vtk")
data_core_r60_reordered = remove_orphan_nodes(data_core_r60)
generate_faces_vtk("JINTRAC_core_reordered_mesh_s134000_r60.vtk",
                   data_core_r60_reordered)

# Replace name of power data 
data_core_r60_reordered["$Q_{rad,tot}[W/m^3]$_cells"] = data_core_r60_reordered["$q[W/m^3]$30.7001007575052_MW_cells"]
data_core_r60_reordered.pop("$q[W/m^3]$30.7001007575052_MW_cells")

# Prepare SOLPS profile to merge with JINTRAC
data_solps_tria = []
for _d_solps in enumerate(data_solps):
    iterator = _d_solps[0]
    _d_solps = _d_solps[1]

    nodes = np.array(_d_solps["nodes"])
    powers = np.array(_d_solps["Q[$W/m^3$]_cells"])
    _d_solps["$Q_{rad,tot}[W/m^3]$_cells"] = powers 
    _d_solps["nodes"] = nodes
    d_solps = convert_quaddata2triadata(_d_solps)
    data_solps_tria.append(d_solps)


###############################################################################
#                           SOLPS + core 60                                   #
###############################################################################
for _d_solps in enumerate(data_solps_tria):
    edge_core_r60 = merge_two_triangular_meshes(_d_solps[1], data_core_r60_reordered)
    edge_core_r60_reordered = remove_double_nodes(edge_core_r60)
    generate_faces_vtk("SOLPS_"+str(shots[_d_solps[0]])+"_JINTRAC_s134000r60_merged.vtk",
                       edge_core_r60_reordered)
