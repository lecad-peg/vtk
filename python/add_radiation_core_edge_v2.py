#! /usr/bin/env python
# This utility adds 2D radiation density information in radiation IDS on basis of core_sources and equilibrium IDS information
#
import imas
import numpy
import sys
import getopt
from copy import deepcopy
from mpl_toolkits.mplot3d import Axes3D
import matplotlib
matplotlib.use('Tkagg')
import matplotlib.pyplot as plt
import matplotlib.path as path
from pylab import *
from scipy import interpolate
import code
from helper_functions import generate_structured_grid_vtk


'''
This utility adds 2D radiation density information in radiation IDS on basis of core_sources and equilibrium IDS information
'''

def open_ids():
    print("Opening IDS")

    imas_obj_in.open_env(user_in, machine_in, version) #open the local database
    imas_obj_out.create_env(user_out, machine_out, version) #open the local database


    if imas_obj_in.isConnected():
        print('input IDS open OK!')
    else:
        print('input IDS open FAILED!')
        sys.exit()
    if imas_obj_out.isConnected():
        print('output IDS open OK!')
    else:
        print('output IDS open FAILED!')
        sys.exit()



def read_ids():

    print('Reading IDS: ')

    print('Copy input to output IDS entries:')
    ids_in_elems = ['equilibrium', 'core_profiles', 'core_transport', 'core_sources', 'summary', 'edge_profiles', 'edge_transport', 'edge_sources', 'dataset_description']
    for elem in ids_in_elems:
        #find number of occurrences in imas_obj_in for each IDS structure
        nooccurrences = 0
        while True:
            eval('imas_obj_in.'+elem+'.get('+str(nooccurrences)+')')
            if eval('imas_obj_in.'+elem+'.ids_properties.homogeneous_time') < 0:
                print(elem+': ',nooccurrences,' occurrences found.')
                break
            else:
                nooccurrences = nooccurrences+1

        #copy all occurrences to imas_obj_out, open last available occurrence for each IDS structure:
        for ioccurrence in range(nooccurrences):
            eval('imas_obj_in.'+elem+'.get('+str(ioccurrence)+')')
            if elem != 'dataset_description':
                print('imas_obj_in '+elem+' code name:',eval('imas_obj_in.'+elem+'.code.name'))
            eval('imas_obj_out.'+elem+'.copyValues( imas_obj_in.'+elem+' )')
            exec('imas_obj_out.'+elem+'.ids_properties.version_put.data_dictionary = ""')
            exec('imas_obj_out.'+elem+'.ids_properties.version_put.access_layer = ""')
            exec('imas_obj_out.'+elem+'.ids_properties.version_put.access_layer_language = ""')
            eval('imas_obj_out.'+elem+'.put('+str(ioccurrence)+')')

    print('Set up radiation IDS data:')
    imas_obj_out.radiation.ids_properties.homogeneous_time = 1
    print(consideredgeradiation)
    if consideredgeradiation == 1:
        timesedgeall = imas_obj_out.edge_sources.time
        #consider edge IDS data that may not be fully populated for each time slice
        times = numpy.array([])
        print("len(timesedgeall)", len(timesedgeall))
        for itidx in range(len(timesedgeall)):
            if len(imas_obj_in.edge_sources.grid_ggd[itidx].space) > 0:
                times = numpy.append(times, timesedgeall[itidx])
    elif considercoreradation == 1:
        times = imas_obj_out.core_sources.time
    else:
        print('Error: consideredgeradiation and/or considercoreradiation need to be set to 1!')
        sys.exit()
    imas_obj_out.radiation.time.resize(len(times))
    print("len(Time):", len(times))

    noradiationprocesses = 2
    imas_obj_out.radiation.grid_ggd.resize(len(times))
    imas_obj_out.radiation.process.resize(noradiationprocesses)
    for iprocess in range(noradiationprocesses):
        imas_obj_out.radiation.process[iprocess].profiles_1d.resize(len(times))
        imas_obj_out.radiation.process[iprocess].ggd.resize(len(times))

    for itidx in range(len(times)):
        imas_obj_out.radiation.time[itidx] = times[itidx]
        print('Prepare radiation IDS time slice data for t = ',times[itidx],' s...')
        if consideredgeradiation == 1:
            itidxedge = (numpy.abs(imas_obj_out.edge_sources.time - times[itidx])).argmin()
            itidxcore = (numpy.abs(imas_obj_out.core_sources.time - times[itidx])).argmin()
        elif considercoreradiation == 1:
            itidxcore = itidx
        itidxcoreprof = (numpy.abs(imas_obj_out.core_profiles.time - times[itidx])).argmin()
        itidxequl = (numpy.abs(imas_obj_out.equilibrium.time - times[itidx])).argmin()
    
        print('Prepare 2D grid structure:')
        if consideredgeradiation == 1:
            coordnum = len(imas_obj_out.edge_sources.grid_ggd[itidxedge].space[0].objects_per_dimension[0].object)
            AllRs = numpy.array([0.])
            AllRs.resize(coordnum)
            AllZs = numpy.array([0.])
            AllZs.resize(coordnum)
            for iidx in range(0,coordnum):
                AllRs[iidx] = imas_obj_out.edge_sources.grid_ggd[itidxedge].space[0].objects_per_dimension[0].object[iidx].geometry[0]
                AllZs[iidx] = imas_obj_out.edge_sources.grid_ggd[itidxedge].space[0].objects_per_dimension[0].object[iidx].geometry[1]
            Rs = numpy.linspace(numpy.min(AllRs), numpy.max(AllRs), RZgridres)
            Zs = numpy.linspace(numpy.min(AllZs), numpy.max(AllZs), RZgridres)
            Rgrid, Zgrid = numpy.meshgrid(Rs, Zs)
        else:
            Rgrid = imas_obj_out.equilibrium.time_slice[itidxequl].profiles_2d[0].r
            Zgrid = imas_obj_out.equilibrium.time_slice[itidxequl].profiles_2d[0].z
            #psishape = numpy.shape(imas_obj_out.equilibrium.time_slice[itidxequl].profiles_2d[0].psi)
        psishape = numpy.shape(Rgrid)
        psisize = size(Rgrid)
        Rgrid1d = numpy.reshape(Rgrid,numpy.size(Rgrid))
        Zgrid1d = numpy.reshape(Zgrid,numpy.size(Zgrid))
    
        if itidx==0: #Write grid_ggd information only for the first time slice to save disk space
            imas_obj_out.radiation.grid_ggd[itidx].identifier.index = 0
            imas_obj_out.radiation.grid_ggd[itidx].space.resize(1)
            imas_obj_out.radiation.grid_ggd[itidx].space[0].identifier.index = 1
            imas_obj_out.radiation.grid_ggd[itidx].space[0].geometry_type.index = 0
            imas_obj_out.radiation.grid_ggd[itidx].space[0].coordinates_type.resize(2)
            imas_obj_out.radiation.grid_ggd[itidx].space[0].coordinates_type[0] = 4 #R coord.
            imas_obj_out.radiation.grid_ggd[itidx].space[0].coordinates_type[1] = 3 #Z coord.
         
            imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension.resize(3)
            imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[0].object.resize(psisize)
            imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[1].object.resize((psishape[0]-1)*psishape[1] + (psishape[1]-1)*psishape[0])
            imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[2].object.resize((psishape[0]-1)*(psishape[1]-1))
    
            imas_obj_out.radiation.grid_ggd[itidx].grid_subset.resize(1)
            imas_obj_out.radiation.grid_ggd[itidx].grid_subset[0].identifier.index = 1
            imas_obj_out.radiation.grid_ggd[itidx].grid_subset[0].element.resize(1)
            imas_obj_out.radiation.grid_ggd[itidx].grid_subset[0].element[0].object.resize(psisize)
    
        imas_obj_out.radiation.process[0].identifier.index = 901
        imas_obj_out.radiation.process[0].identifier.name = "radiation"
        imas_obj_out.radiation.process[0].identifier.description = "Total radiation source"
        imas_obj_out.radiation.process[0].ggd[itidx].electrons.emissivity.resize(1)
        imas_obj_out.radiation.process[0].ggd[itidx].electrons.emissivity[0].values.resize(psisize)
        imas_obj_out.radiation.process[0].ggd[itidx].electrons.emissivity[0].grid_index = 1
        imas_obj_out.radiation.process[0].ggd[itidx].electrons.emissivity[0].grid_subset_index = 1
        imas_obj_out.radiation.process[1].identifier.index = 9
        imas_obj_out.radiation.process[1].identifier.name = "synchrotron_radiation"
        imas_obj_out.radiation.process[1].identifier.description = "Emission from synchrotron radiation"
        imas_obj_out.radiation.process[1].ggd[itidx].electrons.emissivity.resize(1)
        imas_obj_out.radiation.process[1].ggd[itidx].electrons.emissivity[0].values.resize(psisize)
        imas_obj_out.radiation.process[1].ggd[itidx].electrons.emissivity[0].grid_index = 1
        imas_obj_out.radiation.process[1].ggd[itidx].electrons.emissivity[0].grid_subset_index = 1
    #    imas_obj_out.radiation.process[2].identifier.index = 902
    #    imas_obj_out.radiation.process[2].identifier.name = "impurity_radiation"
    #    imas_obj_out.radiation.process[2].identifier.description = "Line radiation and Bremsstrahlung source; radiation losses are negative sources"
    #    imas_obj_out.radiation.process[2].ggd[itidx].electrons.emissivity.resize(1)
    #    imas_obj_out.radiation.process[2].ggd[itidx].electrons.emissivity[0].values.resize(psisize)
    #    imas_obj_out.radiation.process[2].ggd[itidx].electrons.emissivity[0].grid_index = 1
    #    imas_obj_out.radiation.process[2].ggd[itidx].electrons.emissivity[0].grid_subset_index = 1
    
    
    
        psi2d = imas_obj_out.equilibrium.time_slice[itidxequl].profiles_2d[0].psi
        psi2dRgrid = imas_obj_out.equilibrium.time_slice[itidxequl].profiles_2d[0].r
        psi2dZgrid = imas_obj_out.equilibrium.time_slice[itidxequl].profiles_2d[0].z
        psi1d = imas_obj_out.equilibrium.time_slice[itidxequl].profiles_1d.psi
        psi1dorig = psi1d
    
        qrad2d = numpy.copy(psi2d)
        qrad2d[:,:] = 0.0
        qsyr2d = numpy.copy(psi2d)
        qsyr2d[:,:] = 0.0
    
        if consideredgeradiation == 1:
            print('Calculate SOL+div 2D radiation...')
            print('Time associated to edge_sources time slice obtained:')
            print(imas_obj_out.edge_sources.time[itidxedge])
    
            noedgesources = len(imas_obj_out.core_sources.source)
            for edgesourceidx in range(0,noedgesources):
                if imas_obj_out.edge_sources.source[edgesourceidx].identifier.index == 200:
                    print('Radiation source found with index 200..')
                    Allrads = abs(imas_obj_out.edge_sources.source[edgesourceidx].ggd[itidxedge].electrons.energy[0].values) #positive emissivity values to be considered for radiation IDS
            nosubsets = len(imas_obj_out.edge_sources.grid_ggd[itidxedge].grid_subset)
    
            #Find outer boundary contour for SOL+PFR domain:
            idx_cont = numpy.array([0]*7)
            for isub in range(nosubsets):
    #            idindex = imas_obj_out.edge_sources.grid_ggd[itidxedge].grid_subset[isub].identifier.index
    #            print(isub,'. subset with identifier ',imas_obj_out.edge_sources.grid_ggd[itidxedge].grid_subset[isub].identifier.name,' and identifier index no. ',idindex)
                if imas_obj_out.edge_sources.grid_ggd[itidxedge].grid_subset[isub].identifier.index == 18: #outer_baffle
                  idx_cont[0] = isub
                if imas_obj_out.edge_sources.grid_ggd[itidxedge].grid_subset[isub].identifier.index == 17: #main_chamber_wall
                  idx_cont[1] = isub
                if imas_obj_out.edge_sources.grid_ggd[itidxedge].grid_subset[isub].identifier.index == 19: #inner_baffle
                  idx_cont[2] = isub
                if imas_obj_out.edge_sources.grid_ggd[itidxedge].grid_subset[isub].identifier.index == 14: #inner_target
                  idx_cont[3] = isub
                if imas_obj_out.edge_sources.grid_ggd[itidxedge].grid_subset[isub].identifier.index == 21: #inner_pfr_wall
                  idx_cont[4] = isub
                if imas_obj_out.edge_sources.grid_ggd[itidxedge].grid_subset[isub].identifier.index == 20: #outer_pfr_wall
                  idx_cont[5] = isub
                if imas_obj_out.edge_sources.grid_ggd[itidxedge].grid_subset[isub].identifier.index == 13: #outer_target
                  idx_cont[6] = isub
                if imas_obj_out.edge_sources.grid_ggd[itidxedge].grid_subset[isub].identifier.index == 15: #core_boundary
                  idx_cont_core = isub
            objidxs = numpy.array([0])
            spaceidxs = numpy.array([0])
            lineedge1idxs = numpy.array([0])
            lineedge2idxs = numpy.array([0])
            Rcontouts = numpy.array([0.])
            Zcontouts = numpy.array([0.])
            noallcontelempts = 0
            for icontidx in range(len(idx_cont)):
                nocontelempts = len(imas_obj_out.edge_sources.grid_ggd[itidxedge].grid_subset[idx_cont[icontidx]].element) #coordinate output for sub-grids is written element-wise not object-wise with JINTRAC
                objidxs.resize(len(objidxs)+nocontelempts)
                spaceidxs.resize(len(spaceidxs)+nocontelempts)
                lineedge1idxs.resize(len(lineedge1idxs)+nocontelempts)
                lineedge2idxs.resize(len(lineedge2idxs)+nocontelempts)
                Rcontouts.resize(len(Rcontouts)+nocontelempts)
                Zcontouts.resize(len(Zcontouts)+nocontelempts)
                ielemidxrange = range(nocontelempts)
                if icontidx==3 or icontidx==4 or icontidx==5:
                    ielemidxrange = range(nocontelempts-1,-1,-1)
                icuridx = noallcontelempts
                for ielemidx in ielemidxrange:
                    objidxs[icuridx] = imas_obj_out.edge_sources.grid_ggd[itidxedge].grid_subset[idx_cont[icontidx]].element[ielemidx].object[0].index - 1 #object and space indices written in Fortran notation!!
                    spaceidxs[icuridx] = imas_obj_out.edge_sources.grid_ggd[itidxedge].grid_subset[idx_cont[icontidx]].element[ielemidx].object[0].space - 1
                    lineedge1idxs[icuridx] = imas_obj_out.edge_sources.grid_ggd[itidxedge].space[spaceidxs[icuridx]].objects_per_dimension[1].object[objidxs[icuridx]].nodes[0] - 1
                    lineedge2idxs[icuridx] = imas_obj_out.edge_sources.grid_ggd[itidxedge].space[spaceidxs[icuridx]].objects_per_dimension[1].object[objidxs[icuridx]].nodes[1] - 1
                    Rcontouts[icuridx] = imas_obj_out.edge_sources.grid_ggd[itidxedge].space[spaceidxs[icuridx]].objects_per_dimension[0].object[lineedge1idxs[icuridx]].geometry[0]
                    Zcontouts[icuridx] = imas_obj_out.edge_sources.grid_ggd[itidxedge].space[spaceidxs[icuridx]].objects_per_dimension[0].object[lineedge1idxs[icuridx]].geometry[1]
                    icuridx = icuridx + 1
                noallcontelempts = noallcontelempts + nocontelempts
            #remove last contour point:
            Rcontouts = Rcontouts[:-1]
            Zcontouts = Zcontouts[:-1]
            #get boundary contour for innermost core ring:
            nocontcoreelempts = len(imas_obj_out.edge_sources.grid_ggd[itidxedge].grid_subset[idx_cont_core].element) #coordinate output for sub-grids is written element-wise not object-wise with JINTRAC
            Rcontcorebnd = numpy.array([0.]*nocontcoreelempts)
            Zcontcorebnd = numpy.array([0.]*nocontcoreelempts)
            for ielemidx in range(nocontcoreelempts):
                objidxstmp = imas_obj_out.edge_sources.grid_ggd[itidxedge].grid_subset[idx_cont_core].element[ielemidx].object[0].index - 1 #object and space indices written in Fortran notation!!
                spaceidxstmp = imas_obj_out.edge_sources.grid_ggd[itidxedge].grid_subset[idx_cont_core].element[ielemidx].object[0].space - 1
                lineedge1idxstmp = imas_obj_out.edge_sources.grid_ggd[itidxedge].space[spaceidxstmp].objects_per_dimension[1].object[objidxstmp].nodes[0] - 1
                lineedge2idxstmp = imas_obj_out.edge_sources.grid_ggd[itidxedge].space[spaceidxstmp].objects_per_dimension[1].object[objidxstmp].nodes[1] - 1
                Rcontcorebnd[ielemidx] = imas_obj_out.edge_sources.grid_ggd[itidxedge].space[spaceidxstmp].objects_per_dimension[0].object[lineedge1idxstmp].geometry[0]
                Zcontcorebnd[ielemidx] = imas_obj_out.edge_sources.grid_ggd[itidxedge].space[spaceidxstmp].objects_per_dimension[0].object[lineedge1idxstmp].geometry[1]
    
            # map 2D SOL+PFR radiation profile onto 2D grid:
            #qradSOLofRZ = interpolate.interp2d(AllRs,AllZs,Allrads)
            #for iRloc in range(len(Rgridrow)):
            #  for iZloc in range(len(Zgridrow)):
            #    qrad2d[iRloc,iZloc] = qradSOLofRZ(Rgridrow[iRloc],Zgridrow[iZloc])
    
            qrad2dtmp = interpolate.griddata((AllRs,AllZs), Allrads, (Rgrid.ravel(),Zgrid.ravel()))
            qrad2d = numpy.reshape(qrad2dtmp,(len(Rs),len(Zs)))
    
    #        figsol = plt.figure()
    #        ax = figsol.add_subplot(111, projection='3d')
    #        ax.scatter(AllRs,AllZs,Allrads)
    #        plt.show()
    #        code.interact(local=locals())
    #        sys.exit()
    #        ax.plot_trisurf(xx.ravel(),yy.ravel(),qrad2dx)
    
            #create mask for domain bounded by boundary contour for SOL+PFR domain:
            zippedoutbndlist = list(zip(Rcontouts,Zcontouts))
            outbndpath = path.Path(zippedoutbndlist)
            outbndmaskunshaped = outbndpath.contains_points(list(zip(Rgrid.ravel(),Zgrid.ravel())))
            outbndmask = numpy.reshape(outbndmaskunshaped,numpy.shape(Rgrid))
            qrad2d[outbndmask == False] = 0.0
            qsyr2d = qrad2d*0.0
    
    
        if considercoreradiation == 1:
            print('Calculate core 2D radiation...')
            print('Time associated to core_sources time slice obtained:')
            print(imas_obj_out.core_sources.time[itidxcore])

            # get mask for confined region:
            Rcont = imas_obj_out.equilibrium.time_slice[itidxequl].boundary.outline.r
            Zcont = imas_obj_out.equilibrium.time_slice[itidxequl].boundary.outline.z
    
            zippedbndlist = list(zip(Rcont[Rcont > 0.0],Zcont[Rcont > 0.0]))
            seppath = path.Path(zippedbndlist)
            sepmaskunshaped = seppath.contains_points(list(zip(Rgrid1d,Zgrid1d)))
            sepmask = numpy.reshape(sepmaskunshaped,numpy.shape(Rgrid))
     
            # map 2D Psi map data onto 2D grid defined by Rgrid, Zgrid:
            if consideredgeradiation == 1:
                psi2dtmp = interpolate.griddata((psi2dRgrid.ravel(),psi2dZgrid.ravel()), psi2d.ravel(), (Rgrid.ravel(),Zgrid.ravel()), fill_value = 0.0)
                psi2d = numpy.reshape(psi2dtmp,(len(Rs),len(Zs)))
                
            # map 1D radiation profile onto 2D grid:
            maxpsi2d = numpy.max(psi2d)
            minpsi2d = numpy.min(psi2d)
            psi2dnorm = abs(psi2d)
            psi2dnorm[sepmask == False] = 0.0
            psi2dnorm = psi2dnorm/psi2dnorm.max()
            psi2dnorm = 1.0 - psi2dnorm #LCFS: 1.0, on axis: 0.0
            psi1d = numpy.append( maxpsi2d, psi1d[1:] )
            psi1d = numpy.append( psi1d, minpsi2d )
            nosources = len(imas_obj_out.core_sources.source)
            for sourceidx in range(0,nosources):
                if imas_obj_out.core_sources.source[sourceidx].identifier.index == 200:
                    print('Radiation source found with index 200..')
                    qrad1d = abs(imas_obj_out.core_sources.source[sourceidx].profiles_1d[itidxcore].electrons.energy) #positive emissivity values to be considered for radiation IDS
                    qrad1dorig = qrad1d
                    qrad1d = numpy.append( qrad1d[1], qrad1d[1:] )
                    qrad1d = numpy.append( qrad1d , qrad1d[-1])
                if imas_obj_out.core_sources.source[sourceidx].identifier.index == 9:
                    print('Radiation source found with index 9..')
                    qsyr1d = abs(imas_obj_out.core_sources.source[sourceidx].profiles_1d[itidxcore].electrons.energy) #positive emissivity values to be considered for radiation IDS
                    qsyr1dorig = qsyr1d
                    qsyr1d = numpy.append( qsyr1d[1], qsyr1d[1:] )
                    qsyr1d = numpy.append( qsyr1d , qsyr1d[-1])
                if imas_obj_out.core_sources.source[sourceidx].identifier.index == 203:
                    print('Radiation source found with index 203..')
                    qimp1d = abs(imas_obj_out.core_sources.source[sourceidx].profiles_1d[itidxcore].electrons.energy) #positive emissivity values to be considered for radiation IDS
                    qimp1dorig = qimp1d
                    qimp1d = numpy.append( qimp1d[1], qimp1d[1:] )
                    qimp1d = numpy.append( qimp1d , qimp1d[-1])
    
            # determine mapping function qrad(Psi):
            qradofpsi = interpolate.interp1d(psi1d, qrad1d)
            qrad2d[sepmask == True] = qradofpsi(psi2d[sepmask == True])
    
            qsyrofpsi = interpolate.interp1d(psi1d, qsyr1d)
            qsyr2d[sepmask == True] = qsyrofpsi(psi2d[sepmask == True])
    #       qimpofpsi = interpolate.interp1d(psi1d, qimp1d)
    #       qimp2d = numpy.copy(psi2d)
    #       qimp2d[sepmask == True] = qimpofpsi(psi2d[sepmask == True])
    #       qimp2d[sepmask == False] = 0.0
    #       cnt = contour(Rgrid, Zgrid, qrad2d, vmin=abs(qrad2d).min(), vmax=abs(qrad2d).max(), extent=[0, 1, 0, 1])
    
            # Calculate poloidally asymmetric radiation profile:
            if polasymrad == 1:
                qrad2dasym = numpy.copy(qrad2d)
                angf1d = abs(imas_obj_out.core_profiles.profiles_1d[itidxcoreprof].rotation_frequency_tor_sonic)*vtor_multiplier
                angf1dorig = angf1d
                angf1d = numpy.append( angf1d[0], angf1d[0:] )
                angf1d = numpy.append( angf1d , angf1d[-1])
                angfofpsi = interpolate.interp1d(psi1d, angf1d)
                angf2d = numpy.copy(psi2d)
                angf2d[sepmask == True] = angfofpsi(psi2d[sepmask == True])
                angf2d[sepmask == False] = 0.0
                ti1d = imas_obj_out.core_profiles.profiles_1d[itidxcoreprof].t_i_average
                ti1dorig = ti1d
                ti1d = numpy.append( ti1d[0], ti1d[0:] )
                ti1d = numpy.append( ti1d , ti1d[-1])
                tiofpsi = interpolate.interp1d(psi1d, ti1d)
                ti2d = numpy.copy(psi2d)
                ti2d[sepmask == True] = tiofpsi(psi2d[sepmask == True])
                ti2d[sepmask == False] = 1.0
                te1d = imas_obj_out.core_profiles.profiles_1d[itidxcoreprof].electrons.temperature
                te1dorig = ti1d
                te1d = numpy.append( te1d[0], te1d[0:] )
                te1d = numpy.append( te1d , te1d[-1])
                # Look for W ion data index:
                noions = len(imas_obj_out.core_profiles.profiles_1d[itidxcoreprof].ion)
                Widx = -1
                amain = 0.0
                amainno = 0
                for ionidx in range(0,noions):
                    if numpy.size(imas_obj_out.core_profiles.profiles_1d[itidxcoreprof].ion[ionidx].element) == 1:
                        if abs(imas_obj_out.core_profiles.profiles_1d[itidxcoreprof].ion[ionidx].element[0].z_n - 74.0) < 1.0e-6:
                            print('W data included in core_profiles/profiles_1d[itidxcoreprof]/ion[',ionidx,']...')
                            Widx = ionidx
                        if abs(imas_obj_out.core_profiles.profiles_1d[itidxcoreprof].ion[ionidx].element[0].z_n - 1.0) < 1.0e-6:
                            print('Hydrogenic species included in core_profiles/profiles_1d[itidxcoreprof]/ion[',ionidx,']...')
                            amain = amain + imas_obj_out.core_profiles.profiles_1d[itidxcoreprof].ion[ionidx].element[0].a
                            amainno = amainno + 1
                if Widx == -1:
                    print('Error: no W data found in core_profiles/profiles_1d[itidxcoreprof]/ion !')
                    sys.exit()
                if amainno == 0:
                    print('Error: no hydrogenic species data found in core_profiles/profiles_1d[itidxcoreprof]/ion !')
                    sys.exit()
                else:
                   amain = amain / amainno
                   print('Average hydrogenic species mass number: ',amain)
                aW = imas_obj_out.core_profiles.profiles_1d[itidxcoreprof].ion[Widx].element[0].a
                zionavg1d = imas_obj_out.core_profiles.profiles_1d[itidxcoreprof].ion[Widx].z_ion_1d
                zionavg1dorig = zionavg1d
                zionavg1d = numpy.append( zionavg1d[0], zionavg1d[0:] )
                zionavg1d = numpy.append( zionavg1d , zionavg1d[-1])
                mstar1d = aW - zionavg1d * amain * te1d / (te1d + ti1d)
                mstarofpsi = interpolate.interp1d(psi1d, mstar1d)
                mstar2d = numpy.copy(psi2d)
                mstar2d[sepmask == True] = mstarofpsi(psi2d[sepmask == True])
                mstar2d[sepmask == False] = 0.0
                
                expmapraw = numpy.exp(Rgrid**(2.0)*mstar2d*(1.6726e-27/1.602e-19)*angf2d**(2.0)/2.0/ti2d)
                expmap = numpy.copy(expmapraw)
                xpsidiscr = numpy.linspace(xpsires,xpsilim,int(round(xpsilim/xpsires)))
                nxpsidiscr = numpy.size(xpsidiscr)
                idxsconc = numpy.where(psi2dnorm<xpsires)
                if (numpy.size(idxsconc)>0):
                    # expmap[idxsconc] = expmapraw[idxsconc]*(numpy.size(idxsconc)/2.0)/numpy.sum(expmapraw[idxsconc]) #NB: flux surface average over cross-sectional area; size of idxsconc = 2*number of points expmapraw[idxsconc]
                    expmap[idxsconc] = expmapraw[idxsconc]*numpy.sum(Rgrid[idxsconc])/numpy.sum(Rgrid[idxsconc]*expmapraw[idxsconc]) #flux surface average over volume shell
        
                for contidx in range(1,nxpsidiscr):
                    idxsconc = numpy.where((psi2dnorm>=xpsidiscr[contidx-1]) & (psi2dnorm<xpsidiscr[contidx]))
                    if (numpy.size(idxsconc)>0):
                        #expmap[idxsconc] = expmapraw[idxsconc]*(numpy.size(idxsconc)/2.0)/numpy.sum(expmapraw[idxsconc])
                        expmap[idxsconc] = expmapraw[idxsconc]*numpy.sum(Rgrid[idxsconc])/numpy.sum(Rgrid[idxsconc]*expmapraw[idxsconc])
                    else:
                        print('Error: no elements found for xpsidiscr index contidx = ',contidx,' .')
                        print('Try increasing parameter xpsires.')
                        sys.exit()
                idxsconc = numpy.where(psi2dnorm>=xpsilim);
                expmap[idxsconc] = 0.0
                qrad2d[sepmask == True] = qrad2d[sepmask == True] * expmap[sepmask == True]
    
        qrad2d[qrad2d > 1.0e20] = 0.0 #discard extreme radiation peak artifcats
        nohorlines = psishape[1]*(psishape[0]-1)
        for ridx in range(0,psishape[0]):
            for zidx in range(0,psishape[1]):
                idx1d = psishape[1]*ridx+zidx
                if itidx==0:
                    imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[0].object[idx1d].nodes.resize(1)
                    imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[0].object[idx1d].nodes[0] = idx1d + 1 #index in Fortran notation
                    imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[0].object[idx1d].geometry.resize(2)
                #r2dval = imas_obj_out.equilibrium.time_slice[itidxequl].profiles_2d[0].r[ridx,zidx]
                #z2dval = imas_obj_out.equilibrium.time_slice[itidxequl].profiles_2d[0].z[ridx,zidx]
                r2dval = Rgrid[ridx,zidx]
                z2dval = Zgrid[ridx,zidx]
    #            psi2dval = imas_obj_out.equilibrium.time_slice[itidxequl].profiles_2d[0].psi[ridx,zidx]
    
                qrad2dval = qrad2d[ridx,zidx]
                qsyr2dval = qsyr2d[ridx,zidx]
    #            qimp2dval = qimp2d[ridx,zidx]
                if itidx==0:
                    imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[0].object[idx1d].geometry[0] = r2dval
                    imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[0].object[idx1d].geometry[1] = z2dval
    
                    imas_obj_out.radiation.grid_ggd[itidx].grid_subset[0].element[0].object[idx1d].space = 1
                    imas_obj_out.radiation.grid_ggd[itidx].grid_subset[0].element[0].object[idx1d].dimension = 1
                    imas_obj_out.radiation.grid_ggd[itidx].grid_subset[0].element[0].object[idx1d].index = idx1d + 1 #index in Fortran notation
    
                imas_obj_out.radiation.process[0].ggd[itidx].electrons.emissivity[0].values[idx1d] = qrad2dval
                imas_obj_out.radiation.process[1].ggd[itidx].electrons.emissivity[0].values[idx1d] = qsyr2dval
    #            imas_obj_out.radiation.process[2].ggd[itidx].electrons.emissivity[0].values[idx1d] = qimp2dval
    
                if itidx==0:
                    if ridx < psishape[0]-1:
                        imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[1].object[idx1d].nodes.resize(2)
                        imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[1].object[idx1d].nodes[0] = idx1d + 1 #index in Fortran notation
                        imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[1].object[idx1d].nodes[1] = idx1d+psishape[1] + 1 #index in Fortran notation
                        imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[1].object[idx1d].boundary.resize(2)
                        imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[1].object[idx1d].boundary[0].index = idx1d + 1 #index in Fortran notation
                        imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[1].object[idx1d].boundary[1].index = idx1d+psishape[1] + 1 #index in Fortran notation
                    if zidx < psishape[1]-1:
                        objidx = (nohorlines-1)+(psishape[1]-1)*ridx+(zidx+1)
                        imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[1].object[objidx].nodes.resize(2)
                        imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[1].object[objidx].nodes[0] = idx1d + 1 #index in Fortran notation
                        imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[1].object[objidx].nodes[1] = idx1d+1 + 1 #index in Fortran notation
                        imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[1].object[objidx].boundary.resize(2)
                        imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[1].object[objidx].boundary[0].index = idx1d + 1 #index in Fortran notation
                        imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[1].object[objidx].boundary[1].index = idx1d+1 + 1 #index in Fortran notation
    
                        if ridx < psishape[0]-1:
                            objidx2 = (psishape[1]-1)*ridx+(zidx+1) - 1
                            imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[2].object[objidx2].nodes.resize(4)
                            imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[2].object[objidx2].nodes[0] = idx1d + 1 #index in Fortran notation
                            imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[2].object[objidx2].nodes[1] = idx1d+1 + 1 #index in Fortran notation
                            imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[2].object[objidx2].nodes[2] = idx1d+psishape[1] + 1 #index in Fortran notation
                            imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[2].object[objidx2].nodes[3] = idx1d+psishape[1]+1 + 1 #index in Fortran notation
                            imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[2].object[objidx2].boundary.resize(4)
                            imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[2].object[objidx2].boundary[0].index = idx1d + 1 #index in Fortran notation
                            if zidx > 0:
                                 imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[2].object[objidx2].boundary[0].neighbours.resize(1) 
                                 imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[2].object[objidx2].boundary[0].neighbours[0] = objidx2-1 + 1 #index in Fortran notation
                            imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[2].object[objidx2].boundary[1].index = objidx + 1 #index in Fortran notation
                            if ridx > 0:
                                 imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[2].object[objidx2].boundary[1].neighbours.resize(1) 
                                 imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[2].object[objidx2].boundary[1].neighbours[0] = objidx2-(psishape[1]-1) + 1 #index in Fortran notation
                            imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[2].object[objidx2].boundary[2].index = idx1d+1 + 1 #index in Fortran notation
                            if zidx < psishape[1]-2:
                                 imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[2].object[objidx2].boundary[2].neighbours.resize(1) 
                                 imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[2].object[objidx2].boundary[2].neighbours[0] = objidx2+1 + 1 #index in Fortran notation
                            imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[2].object[objidx2].boundary[3].index = objidx+(psishape[1]-1) + 1 #index in Fortran notation
                            if ridx < psishape[0]-2:
                                 imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[2].object[objidx2].boundary[3].neighbours.resize(1) 
                                 imas_obj_out.radiation.grid_ggd[itidx].space[0].objects_per_dimension[2].object[objidx2].boundary[3].neighbours[0] = objidx2+(psishape[1]-1) + 1 #index in Fortran notation
    
        # set up 1d profiles:
        imas_obj_out.radiation.process[0].profiles_1d[itidx].grid.rho_tor_norm = imas_obj_out.equilibrium.time_slice[0].profiles_1d.rho_tor_norm[1:]
        imas_obj_out.radiation.process[0].profiles_1d[itidx].grid.rho_tor = imas_obj_out.equilibrium.time_slice[0].profiles_1d.rho_tor[1:]
        imas_obj_out.radiation.process[0].profiles_1d[itidx].grid.volume = imas_obj_out.equilibrium.time_slice[0].profiles_1d.volume[1:]
        imas_obj_out.radiation.process[0].profiles_1d[itidx].grid.area = imas_obj_out.equilibrium.time_slice[0].profiles_1d.area[1:]
        imas_obj_out.radiation.process[0].profiles_1d[itidx].grid.psi = psi1dorig[1:]
        if considercoreradiation == 1:
            imas_obj_out.radiation.process[0].profiles_1d[itidx].electrons.emissivity = qrad1dorig[1:]
    
    #    imas_obj_out.radiation.process[1].profiles_1d.resize(1)
        imas_obj_out.radiation.process[1].profiles_1d[itidx] = deepcopy(imas_obj_out.radiation.process[0].profiles_1d[itidx])
        if considercoreradiation == 1:
            imas_obj_out.radiation.process[1].profiles_1d[itidx].electrons.emissivity = qsyr1dorig[1:]
    #    imas_obj_out.radiation.process[2].profiles_1d[itidx] = deepcopy(imas_obj_out.radiation.process[0].profiles_1d[itidx])
    #    imas_obj_out.radiation.process[2].profiles_1d[0].electrons.emissivity = qimp1dorig[1:]

    imas_obj_out.radiation.put()

    print('max(qrad2d):',qrad2d.max().max())

    generate_structured_grid_vtk(Rgrid, Zgrid, qrad2d,
                                 "JINTRAC_radiation_shot"+str(shot_out)+"_run"+str(run_out)+".vtk")


#if __name__ == '__main__':
version = "3"
shot_in = 134000
run_in = 37
# shot_in = 134174
# run_in = 117
user_in = "public"
machine_in = "iter"

shot_out = 134010
run_out = 37
# shot_out = 134174
# run_out = 127
# user_out = "koechlf"
# machine_out = "iter"

consideredgeradiation = 1
considercoreradiation = 1 #apply radiation in confined region from 1D radiation data in core_profiles
polasymrad = 0 #polasymrad = 0: map 1D radiation profiles assuming poloidal symmetry; polasymrad = 1: map 1D radiation profiles with assumption of poloidal asymmetry, applying asymmetry factor as given in T. Koskela et al. PPCF 2015 57 045001
#    RZgridres = 250 #resolution in R and Z direction of (R,Z) grid in use for calculation of 2D distribution of radiation that is written to radiation IDS
RZgridres = 500
xpsires = 0.02 #resolution in Psi_norm for application of normalisation factor to poloidal asymmetry weighting term
xpsilim = 0.9999 #limit in Psi_norm for consideration of volume shells in the confined region for application of normalisation factor to poloidal asymmetry weighting term
vtor_multiplier = 1.0 #should be set to 1.0 unless simple scans in v_tor assumptions need to be carried out


user_out = "brankm"
machine_out = "iter"

shot_list = [134000]
run_list = [60]

for s in shot_list:
    for r in run_list:
        try:
            shot_in = s
            run_in = r
            shot_out = s
            run_out = r
            imas_obj_in = imas.ids(shot_in, run_in, shot_in, run_in)
            imas_obj_out = imas.ids(shot_out, run_out, shot_out, run_out)
            open_ids()
            read_ids()
            imas_obj_in.close()
            imas_obj_out.close()
        except:
            continue


