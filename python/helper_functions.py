import math
import numpy as np
import copy

def split_quad_to_trias(nodes, node_ids):
    A = nodes[0]
    B = nodes[1]
    C = nodes[2]
    D = nodes[3]

    A_id = node_ids[0]
    B_id = node_ids[1]
    C_id = node_ids[2]
    D_id = node_ids[3]

    len_AC = np.sqrt((A[0]-C[0])**2+(A[1]-C[1])**2)
    len_BD = np.sqrt((B[0]-D[0])**2+(B[1]-D[1])**2)
    if len_AC < len_BD:
        trias = [[[A, B, C], [A, C, D]], [[A_id, B_id, C_id], [A_id, C_id, D_id]]]
        return trias
    elif len_BD < len_AC:
        trias = [[[A, B, D], [D, B, C]], [[A_id, B_id, D_id], [D_id, B_id, C_id]]]
        return trias


def check_clockwise_order(nodes, cells):
    # check if node IDs in cell are defined in clockwise or counter
    # clockwise order
    nodes = np.array(nodes).astype('float')
    cells = np.array(cells).astype('int')

    a = nodes[cells[:,0]]
    b = nodes[cells[:,1]]
    c = nodes[cells[:,2]]
    d = nodes[cells[:,3]]

    # check counter-clockwise
    a2b = (b[:, 0]-a[:, 0])*(b[:, 1]+a[:, 1])
    b2c = (c[:, 0]-b[:, 0])*(c[:, 1]+b[:, 1])
    c2d = (d[:, 0]-c[:, 0])*(d[:, 1]+c[:, 1])
    d2a = (a[:, 0]-d[:, 0])*(a[:, 1]+d[:, 1])
    sum_cc = a2b+b2c+c2d+d2a
    sign = np.zeros(cells.shape[0])
    for _sum in enumerate(sum_cc):
        _sum_id = _sum[0]
        _sum = _sum[1]
        if _sum > 0:
            sign[_sum_id] = -1
        else:
            sign[_sum_id] = 1

    return sign

def convert_quad2tria(cells):
    # Cells have indices in clockwise/counterclocwise order
    cells_tria = []
    for cell in cells:
        cells_tria.append([cell[0], cell[1] ,cell[2]])
        cells_tria.append([cell[0], cell[2], cell[3]])

    return cells_tria

def convert_quaddata2triadata(_data):
    cells = _data["cells"]
    data_cells_tria = copy.deepcopy(_data)
    cells_tria = convert_quad2tria(cells)
    for name in data_cells_tria:
        data = data_cells_tria[name]
        if len(data) == len(cells):
            data_tria = []
            for value in data:
                data_tria.append(value)
                data_tria.append(value)

            data_cells_tria[name] = data_tria

    data_cells_tria["cells"] = cells_tria
    return data_cells_tria


def convert_node_data_to_cell_data(nodes, cells, node_data):
    nodes = np.array(nodes).astype('float')
    cells = np.array(cells).astype('int')
    cell_data = np.zeros(cells.shape[0])
    for cell in enumerate(cells):
        cell_id = cell[0]
        cell = cell[1]
        q_avg = np.sum(np.fromiter([node_data[cell[i]] for i in range(cell.shape[0])],
                                   dtype='float'))/cells.shape[1]
        cell_data[cell_id] = q_avg

    return cell_data


def cell_centroid(nodes, cells):
    # returns array of centroids of quadrangles
    # input array of nodes and arrai of quadrangle indices
    # The cell must line in x, y plane
    nodes = np.array(nodes).astype('float')
    cells = np.array(cells).astype('int')
    if cells.shape[1] == 3:
        centroids_x = []
        centroids_y = []
        for cell in cells:
            x0 = nodes[cell[0]][0]
            y0 = nodes[cell[0]][1]
            x1 = nodes[cell[1]][0]
            y1 = nodes[cell[1]][1]
            x2 = nodes[cell[2]][0]
            y2 = nodes[cell[1]][1]
            centroid = calculate_barycenter(x0, y0, x1, y1, x2, y2)
            centroids_x.append(centroid[0])
            centroids_y.append(centroid[1])
        return [np.array(centroids_x),
                np.array(centroids_y)]

    a = nodes[cells[:,0]]
    b = nodes[cells[:,1]]
    c = nodes[cells[:,2]]
    d = nodes[cells[:,3]]

    sm0_x = ((a[:, 0]+b[:, 0])*(a[:, 0]*b[:, 1]-b[:, 0]*a[:, 1]))
    sm1_x = ((b[:, 0]+c[:, 0])*(b[:, 0]*c[:, 1]-c[:, 0]*b[:, 1]))
    sm2_x = ((c[:, 0]+d[:, 0])*(c[:, 0]*d[:, 1]-d[:, 0]*c[:, 1]))
    sm3_x = ((d[:, 0]+a[:, 0])*(d[:, 0]*a[:, 1]-a[:, 0]*d[:, 1]))

    sm0_y = ((a[:, 1]+b[:, 1])*(a[:, 0]*b[:, 1]-b[:, 0]*a[:, 1]))
    sm1_y = ((b[:, 1]+c[:, 1])*(b[:, 0]*c[:, 1]-c[:, 0]*b[:, 1]))
    sm2_y = ((c[:, 1]+d[:, 1])*(c[:, 0]*d[:, 1]-d[:, 0]*c[:, 1]))
    sm3_y = ((d[:, 1]+a[:, 1])*(d[:, 0]*a[:, 1]-a[:, 0]*d[:, 1]))

    area = cells_area(nodes, cells)
    sign = check_clockwise_order(nodes, cells)
    Cx = 1/(6*area)*(sm0_x+sm1_x+sm2_x+sm3_x)*sign
    Cy = 1/(6*area)*(sm0_y+sm1_y+sm2_y+sm3_y)*sign
    return [Cx, Cy]

def quadrangle_centroid_shoelace_area(pt_x, pt_y):
    # calculation of area and centroid of quadrangle based on shoelace
    # algorithm
    a = [pt_x[0], pt_y[0]]
    b = [pt_x[1], pt_y[1]]
    c = [pt_x[2], pt_y[2]]
    d = [pt_x[3], pt_y[3]]

    sm0_x = ((a[0]+b[0])*(a[0]*b[1]-b[0]*a[1]))
    sm1_x = ((b[0]+c[0])*(b[0]*c[1]-c[0]*b[1]))
    sm2_x = ((c[0]+d[0])*(c[0]*d[1]-d[0]*c[1]))
    sm3_x = ((d[0]+a[0])*(d[0]*a[1]-a[0]*d[1]))
    #sm3_x = 0
    sm0_y = ((a[1]+b[1])*(a[0]*b[1]-b[0]*a[1]))
    sm1_y = ((b[1]+c[1])*(b[0]*c[1]-c[0]*b[1]))
    sm2_y = ((c[1]+d[1])*(c[0]*d[1]-d[0]*c[1]))
    sm3_y = ((d[1]+a[1])*(d[0]*a[1]-a[0]*d[1]))
    #sm3_y = 0

    area = shoelace_area(pt_x, pt_y)
    Cx = 1/(6*area)*(sm0_x+sm1_x+sm2_x+sm3_x)
    Cy = 1/(6*area)*(sm0_y+sm1_y+sm2_y+sm3_y)
    return [Cx, Cy]


def cells_area(nodes, cells):
    # based on shoelace algorithm
    nodes = np.array(nodes).astype('float')
    cells = np.array(cells).astype('int')

    # Check if cell is a triangle
    if cells.shape[1] == 3:
        a = nodes[cells[:, 0]]
        #print("aaa", a)
        b = nodes[cells[:, 1]]
        #print("bbb", b)
        c = nodes[cells[:, 2]]
        #print("ccc", c)
        areas = calculate_tria_area(a[:, 0], a[:, 1],
                                    b[:, 0], b[:, 1],
                                    c[:, 0], c[:, 1])

        return areas

    a = nodes[cells[:,0]]
    b = nodes[cells[:,1]]
    c = nodes[cells[:,2]]
    d = nodes[cells[:,3]]

    area = np.zeros(len(cells))
    for cell in enumerate(cells):
        cell_ind = cell[0]
        cell = cell[1]
        x_list = [nodes[cell[0]][0],
                  nodes[cell[1]][0],
                  nodes[cell[2]][0],
                  nodes[cell[3]][0]]
        y_list = [nodes[cell[0]][1],
                  nodes[cell[1]][1],
                  nodes[cell[2]][1],
                  nodes[cell[3]][1]]
        a1, a2 = 0, 0
        x_list.append(x_list[0])
        y_list.append(y_list[0])
        for j in range(len(x_list)-1):
            a1 += x_list[j]*y_list[j+1]
            a2 += y_list[j]*x_list[j+1]
        l=abs(a1-a2)/2
        area[cell_ind] = l

    return np.array(area)


def shoelace_area(x_list, y_list):
    # based on shoelace algorithm
    a1, a2 = 0, 0
    x_list.append(x_list[0])
    y_list.append(y_list[0])
    for j in range(len(x_list)-1):
        a1 += x_list[j]*y_list[j+1]
        a2 += y_list[j]*x_list[j+1]
    l=abs(a1-a2)/2
    return l

def calculate_node_distance(pt0, pt1):
    return math.sqrt((pt0[0]-pt1[0])**2+(pt0[1]-pt1[1])**2)

def calculate_max_quad_area(a , b , c , d ):
    # Calculating the semi-perimeter
    # of the given quadrilateral
    semiperimeter = (a + b + c + d) / 2

    # Applying Brahmagupta's formula to
    # get maximum area of quadrilateral
    return math.sqrt((semiperimeter - a) *
                    (semiperimeter - b) *
                    (semiperimeter - c) *
                    (semiperimeter - d))

def calculate_barycenter(x1, y1, x2, y2, x3, y3):
    return [(x1+x2+x3)/3, (y1+y2+y3)/3]

def calculate_tria_area(x1, y1, x2, y2, x3, y3):
    return np.abs(x1*y2+x2*y3+x3*y1-y1*x2-y2*x3-y3*x1)/2

def find_centroid_of_4_coordinates(a, b, c, d):
    # find the centroid of triangle 1
    x1 = (a[0]+b[0]+d[0])/3
    y1 = (a[1]+b[1]+d[1])/3
    z1 = (a[2]+b[2]+d[2])/3
    # find the centroid of triangle 2
    x2 = (b[0]+c[0]+d[0])/3
    y2 = (b[1]+c[1]+d[1])/3
    z2 = (b[2]+c[2]+d[2])/3
    # find the midpoint of line between the centroids of triangle 1
    # and 2
    xc = (x1+x2)/2
    yc = (y1+y2)/2
    zc = (z1+z2)/2
    return xc, yc, zc


def generate_faces_vtk(file_name, vtk_data):

    vtk_data = copy.deepcopy(vtk_data)

    nodes = vtk_data["nodes"]
    faces = vtk_data["cells"]
    vtk_data.pop("nodes")
    vtk_data.pop("cells")
    node_data = {}
    face_data = {}
    for name in vtk_data:
        if "_nodes" in name:
            node_data[name] = vtk_data[name]
        else:
            face_data[name] = vtk_data[name]

    n_nodes = len(nodes)
    n_faces = len(faces)
    face_type = 5
    if len(faces[0]) == 3:
        face_type = 5
    elif len(faces[0]) == 4:
        face_type = 9
    with open(file_name, "w") as f:
        f.write("# vtk DataFile Version 3.0\n")
        f.write("Mesh_1\n")
        f.write("ASCII\n")
        f.write("DATASET UNSTRUCTURED_GRID\n")
        f.write("POINTS "+str(n_nodes)+" float\n")
        for node in nodes:
            f.write(str(node[0])+" "+str(node[1])+" "+str(node[2])+"\n")

        f.write("\n")
        f.write("CELLS "+str(n_faces)+" "+str(n_faces*(len(faces[0])+1))+"\n")

        faces = np.array(faces, dtype='int')
        if face_type == 5:
            for face in faces:
                f.write("3 "+
                        str(face[0])+" "+
                        str(face[1])+" "+
                        str(face[2])+"\n")
        elif face_type == 9:
            for face in faces:
                f.write("4 "+
                        str(face[0])+" "+
                        str(face[1])+" "+
                        str(face[2])+" "+
                        str(face[3])+"\n")

        f.write("\n")
        f.write("CELL_TYPES "+str(n_faces)+"\n")
        for i in range(n_faces):
            f.write(str(face_type)+"\n")

        for data in enumerate(node_data):
            iterator = data[0]
            val_name = data[1]
            f.write("\n")
            if iterator == 0:
                f.write("POINT_DATA "+str(n_nodes)+"\n")

            f.write("SCALARS "+val_name.replace(" ", "")+" float 1\n")
            f.write("LOOKUP_TABLE default\n")
            for value in node_data[val_name]:
                f.write("  "+str(value)+"\n")

        for data in enumerate(face_data):
            iterator = data[0]
            val_name = data[1]
            f.write("\n")
            if iterator == 0:
                f.write("CELL_DATA "+str(n_faces)+"\n")

            f.write("SCALARS "+val_name.replace(" ", "")+" float 1\n")
            f.write("LOOKUP_TABLE default\n")
            for value in face_data[val_name]:
                f.write("  "+str(value)+"\n")


def generate_edges_vtk(file_name, vtk_data):

    vtk_data = copy.deepcopy(vtk_data)

    nodes = vtk_data["nodes"]
    edges = vtk_data["cells"]
    vtk_data.pop("nodes")
    vtk_data.pop("cells")
    node_data = {}
    edge_data = {}
    for name in vtk_data:
        if "_nodes" in name:
            node_data[name] = vtk_data[name]
        else:
            edge_data[name] = vtk_data[name]

    n_nodes = len(nodes)
    n_edges = len(edges)
    edge_type = 3

    with open(file_name, "w") as f:
        f.write("# vtk DataFile Version 3.0\n")
        f.write("Mesh_1\n")
        f.write("ASCII\n")
        f.write("DATASET UNSTRUCTURED_GRID\n")
        f.write("POINTS "+str(n_nodes)+" float\n")
        for node in nodes:
            f.write(str(node[0])+" "+str(node[1])+" "+str(node[2])+"\n")

        f.write("\n")
        f.write("CELLS "+str(n_edges)+" "+str(n_edges*(2+1))+"\n")

        for edge in edges:
            f.write("2 "+
                    str(edge[0])+" "+
                    str(edge[1])+"\n")

        f.write("CELL_TYPES "+str(n_edges)+"\n")
        for i in range(n_edges):
            f.write("3\n")

        for data in enumerate(node_data):
            iterator = data[0]
            val_name = data[1]
            f.write("\n")
            if iterator == 0:
                f.write("POINT_DATA "+str(n_nodes)+"\n")

            f.write("SCALARS "+val_name.replace(" ", "")+" float 1\n")
            f.write("LOOKUP_TABLE default\n")
            for value in node_data[val_name]:
                f.write("  "+str(value)+"\n")

        for data in enumerate(edge_data):
            iterator = data[0]
            val_name = data[1]
            f.write("\n")
            if iterator == 0:
                f.write("CELL_DATA "+str(n_edges)+"\n")

            f.write("SCALARS "+val_name.replace(" ", "")+" float 1\n")
            f.write("LOOKUP_TABLE default\n")
            for value in edge_data[val_name]:
                f.write("  "+str(value)+"\n")


def generate_points_vtk(file_name, vtk_data):
    vtk_data = copy.deepcopy(vtk_data)

    nodes = vtk_data["nodes"]
    vtk_data.pop("nodes")
    node_data = {}
    for name in vtk_data:
        if "_nodes" in name:
            node_data[name] = vtk_data[name]

    n_nodes = len(nodes)

    with open(file_name, "w") as f:
        f.write("# vtk DataFile Version 3.0\n")
        f.write("Mesh_1\n")
        f.write("ASCII\n")
        f.write("DATASET UNSTRUCTURED_GRID\n")
        f.write("POINTS "+str(n_nodes)+" float\n")
        for node in nodes:
            f.write(str(node[0])+" "+str(node[1])+" "+str(node[2])+"\n")

        for data in enumerate(node_data):
            iterator = data[0]
            val_name = data[1]
            f.write("\n")
            if iterator == 0:
                f.write("POINT_DATA "+str(n_nodes)+"\n")

            f.write("SCALARS "+val_name.replace(" ", "")+" float 1\n")
            f.write("LOOKUP_TABLE default\n")
            for value in node_data[val_name]:
                f.write("  "+str(value)+"\n")


def read_vtk(file_name):

    data = {}
    with open(file_name, "r") as f:
        f.readline()
        f.readline()
        f.readline()
        line = f.readline()
        if line == "\n":
            f.readline()
        n_pts = int(f.readline().split()[1])
        nodes = []
        for node in range(n_pts):
            nodes.append([float(coord) for coord in f.readline().split()])

        nodes = np.array(nodes)
        node0 = nodes[0, :]
        #print(nodes[:, 0])
        if node0[1] == 0:
            nodes = np.array([nodes[:, 0], nodes[:, 1], nodes[:, 2]])
            nodes = nodes.T.tolist()

        #print(nodes)
        data["nodes"] = nodes

        line = f.readline()
        if line == '\n':
            line = f.readline()

        n_cells = int(line.split()[1])

        cells = [];
        for cell in range(n_cells):
            cells.append([float(cell_id) for cell_id in f.readline().split()[1:]])

        data["cells"] = cells
        line = f.readline()
        if line == '':
            line = f.readline()

        for cell in range(n_cells):
            f.readline()

        point_data = False
        cell_data = False

        for ln in f:
            line = f.readline()
            if line == '\n':
                line = f.readline()

            if line.split()[0] == "POINT_DATA":
                point_data = True
                cell_data = False
                data_name = f.readline().split()[1]

            elif line.split()[0] == "CELL_DATA":
                point_data = False
                cell_data = True
                data_name = f.readline().split()[1]

            elif line.split()[0] == "SCALARS":
                data_name = line.split()[1]

            f.readline()

            if point_data:
                data_set = []
                for value in range(n_pts):
                    data_set.append(float(f.readline().split()[0]))

                if "_nodes" in data_name:
                    data[data_name] = data_set
                else:
                    data[data_name+"_nodes"] = data_set
                point_data = True
                cell_data = False

            elif cell_data:
                data_set = []
                for value in range(n_cells):
                    data_set.append(float(f.readline().split()[0]))

                if "_cells" in data_name:
                    data[data_name] = data_set
                else:
                    data[data_name+"_cells"] = data_set

                point_data = False
                cell_data = True

    return data


def remove_orphan_nodes(data):

    data = copy.deepcopy(data)
    nodes = np.array(data["nodes"]).astype("float")
    cells = np.array(data["cells"]).astype("int")
    new_cells = np.zeros(cells.shape)
    old_nodes_ids = np.isin(np.arange(nodes.shape[0]), cells).astype('int')
    old_nodes_ids = np.argwhere(old_nodes_ids).T[0]
    new_nodes = nodes[old_nodes_ids]
    for id_nr in enumerate(old_nodes_ids):
        new_cells[np.where(cells==id_nr[1])] = id_nr[0]

    data["nodes"] = new_nodes
    data["cells"] = new_cells
    for name in data:
        if "_nodes" in data:
            data[name] = data[name][old_nodes_ids]

    return data


def merge_two_triangular_meshes(vtk_data0, vtk_data1):

    vtk_data0 = copy.deepcopy(vtk_data0)
    vtk_data1 = copy.deepcopy(vtk_data1)

    nodes0 = np.array(vtk_data0["nodes"]).astype('float')
    n_nodes0 = nodes0.shape[0]
    cells0 = np.array(vtk_data0["cells"]).astype('int')
    n_cells0 = cells0.shape[0]

    vtk_data0.pop("nodes")
    vtk_data0.pop("cells")

    nodes1 = np.array(vtk_data1["nodes"]).astype('float')
    n_nodes1 = nodes1.shape[0]
    cells1 = np.array(vtk_data1["cells"]).astype('int')
    n_cells1 = cells1.shape[0]

    vtk_data1.pop("nodes")
    vtk_data1.pop("cells")

    new_nodes = np.concatenate((nodes0, nodes1))
    new_cells = np.concatenate((cells0, cells1+n_nodes0))

    new_data = {"nodes":new_nodes, "cells":new_cells}

    for name in vtk_data0:
        if name in vtk_data1:
            new_data[name] = np.concatenate((vtk_data0[name], vtk_data1[name]))
            vtk_data1.pop(name)
        elif "_nodes" in name:
            new_data[name] = np.concatenate((vtk_data0[name], np.zeros(n_nodes1)))
        elif "_cells" in name:
            new_data[name] = np.concatenate((vtk_data0[name], np.zeros(n_cells1)))


    for name in vtk_data1:
        if "_nodes" in name:
            new_data[name] = np.concatenate((np.zeros(n_nodes0), vtk_data1[name]))
        elif "_cells" in name:
            new_data[name] = np.concatenate((np.zeros(n_cells0), vtk_data1[name]))

    return new_data

def remove_double_nodes(data):

    data = copy.deepcopy(data)
    nodes = np.array(data["nodes"]).astype('float')
    cells = np.array(data["cells"]).astype('int')

    data.pop("nodes")
    data.pop("cells")

    # Find repeating nodes
    for nd in enumerate(nodes):
        id_repeating_nodes = np.where(np.all(np.isclose(nd[1], nodes), axis=1)==True)
        if id_repeating_nodes[0].shape[0] == 2:
            #new_nodes.append(id_repeating_nodes)
            cells[cells == id_repeating_nodes[0][1]] = id_repeating_nodes[0][0]

    # reorder nodes and remove new orphan nodes, as well as data on them
    new_cells = np.zeros(cells.shape)
    old_nodes_ids = np.isin(np.arange(nodes.shape[0]), cells).astype('int')
    old_nodes_ids = np.argwhere(old_nodes_ids).T[0]
    new_nodes = nodes[old_nodes_ids]
    data["nodes"] = new_nodes
    for name in data:
        if "_nodes" in name:
            data[name] = data[name][old_nodes_ids]
            print("    Data of "+str(name)+" on double nodes is deleted.")

    for id_nr in enumerate(old_nodes_ids):
        new_cells[np.where(cells==id_nr[1])] = id_nr[0]

    data["cells"] = new_cells

    return data


def generate_structured_grid_vtk(rgrid2d, zgrid2d, qrad2d, vtk_file):
    r = np.ndarray.flatten(rgrid2d)
    z = np.ndarray.flatten(zgrid2d)
    qrad = np.ndarray.flatten(qrad2d)
    n_pts = len(r)
    with open(vtk_file, "w") as f:
        f.write("# vtk DataFile Version 3.0\n")
        f.write("Mesh_1\n")
        f.write("ASCII\n")
        f.write("DATASET UNSTRUCTURED_GRID\n")
        f.write("POINTS "+str(n_pts)+" float\n")
        for i in range(n_pts):
            f.write(str(r[i])+" 0 "+str(z[i])+"\n")

        f.write("\n")
        n_cls = (rgrid2d.shape[0]-1)*(rgrid2d.shape[1]-1)
        f.write("CELLS "+str(n_cls)+" "+str(n_cls*5)+"\n")
        for j in range(rgrid2d.shape[0]-1):
            for i in range(rgrid2d.shape[1]-1):
                f.write(" 4 "+str(j*rgrid2d.shape[1]+i)+
                        " "+str(j*rgrid2d.shape[1]+i+1)+
                        " "+str((j+1)*rgrid2d.shape[1]+i+1)+
                        " "+str((j+1)*rgrid2d.shape[1]+i)+"\n")

        f.write("\n")
        f.write("CELL_TYPES "+str(n_cls)+"\n")
        for i in range(n_cls):
            f.write(" 9\n")

        f.write("\n")
        f.write("POINT_DATA "+str(n_pts)+"\n")
        f.write("SCALARS q float 1\n")
        f.write("LOOKUP_TABLE default\n")
        for i in qrad:
            f.write(str(i)+"\n")

        f.write("\n")
        f.write("CELL_DATA "+str(n_cls)+"\n")
        f.write("SCALARS q float 1\n")
        f.write("LOOKUP_TABLE default\n")
        for j in range(rgrid2d.shape[0]-1):
            for i in range(rgrid2d.shape[1]-1):
                q00 = qrad[j*rgrid2d.shape[1]+i]
                q10 = qrad[j*rgrid2d.shape[1]+i+1]
                q11 = qrad[(j+1)*rgrid2d.shape[1]+i+1]
                q01 = qrad[(j+1)*rgrid2d.shape[1]+i]
                f.write(" "+str((q00+q10+q11+q01)/4)+"\n")
