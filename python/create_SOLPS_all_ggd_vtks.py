from helper_functions import generate_faces_vtk,generate_edges_vtk, generate_points_vtk
from helper_functions import find_centroid_of_4_coordinates, calculate_barycenter
from helper_functions import calculate_tria_area, calculate_max_quad_area
from helper_functions import calculate_node_distance, shoelace_area
from helper_functions import cell_centroid
import imas
import numpy as np
import os

# Define IDS parameters
username = "lorej"
device = "ITER"
shots = [123275, 123276, 123277, 123278, 123279]
runs = [1]

# Define folder to save VTK data from IDS
ggd_folder = "./all_ggd_vtks"
if not os.path.isdir(ggd_folder):
    os.mkdir(ggd_folder)

for shot_enum in enumerate(shots):
    for run_enum in enumerate(runs):

        shot_iterator = shot_enum[0]
        run_iterator = run_enum[0]
        shot = shot_enum[1]
        run = run_enum[1]
        shot_run = "shot"+str(shot)+"_run"+str(run)
        imas_ids = imas.ids(shot, run)
        imas_ids.open_env(username, device, "3")
        imas_ids.edge_profiles.get()
        imas_ids.radiation.get()
        edge_profiles_ids = imas_ids.edge_profiles
        radiation_ids = imas_ids.radiation
        ids_to_plot = radiation_ids

        # get number of SOLPS nodes, edges, faces
        nr_nodes = len(ids_to_plot.grid_ggd[0].space[0].objects_per_dimension[0].object.array)
        nr_edges = len(ids_to_plot.grid_ggd[0].space[0].objects_per_dimension[1].object.array)
        nr_faces = len(ids_to_plot.grid_ggd[0].space[0].objects_per_dimension[2].object.array)

        # 0d
        pts = []
        for node_id in range(nr_nodes):
            r = ids_to_plot.grid_ggd[0].space[0].objects_per_dimension[0].object[node_id].geometry[0]
            z = ids_to_plot.grid_ggd[0].space[0].objects_per_dimension[0].object[node_id].geometry[1]
            pts.append([r, 0, z])
        # 1d
        edges = []
        for edge_id in range(nr_edges):
            edges.append(ids_to_plot.grid_ggd[0].space[0].objects_per_dimension[1].object[edge_id].nodes)
        # 2d
        faces = []
        calculated_face_area = []
        calculated_face_center_r = []
        calculated_centroid_r = []
        for face_id in range(nr_faces):
            face_node_ids = ids_to_plot.grid_ggd[0].space[0].objects_per_dimension[2].object[face_id].nodes
            faces.append(face_node_ids)
            if len(face_node_ids) == 4:
                face_node_0 = pts[face_node_ids[0]-1]
                face_node_1 = pts[face_node_ids[1]-1]
                face_node_2 = pts[face_node_ids[2]-1]
                face_node_3 = pts[face_node_ids[3]-1]
                r_list = [face_node_0[0], face_node_1[0], face_node_2[0], face_node_3[0]]
                z_list = [face_node_0[2], face_node_1[2], face_node_2[2], face_node_3[2]]
                _area = shoelace_area(r_list, z_list)
                calculated_face_area.append(_area)
                calculated_face_center_r.append(sum(r_list)/4)
                [Cx, Cy] = cell_centroid(np.array([r_list, z_list,
                                                   [0 for i in range(len(r_list))]]).T,
                                         [[0,1,2,3]])
                calculated_centroid_r.append(0)

        calculated_face_center_r = np.array(calculated_face_center_r)
        calculated_centroid_r = np.array(calculated_centroid_r)
        #get face area
        face_area = []
        for area in ids_to_plot.grid_ggd[0].space[0].objects_per_dimension[2].object:
            face_area.append(area.measure)

        face_area = np.array(face_area)
        cell_volume = face_area*2*np.pi*calculated_face_center_r
        grid_subset_size = len(ids_to_plot.grid_ggd[0].grid_subset)
        for i in range(grid_subset_size):
            name = ids_to_plot.grid_ggd[0].grid_subset[i].identifier.name.replace(" ", "")
            dimension = ids_to_plot.grid_ggd[0].grid_subset[i].dimension
            cell_indices = []
            subset_id = i
            subset = ids_to_plot.grid_ggd[0].grid_subset[i]
            point_data = {}
            cell_data = {}

            if dimension == 3:
                cell_area = []
                calculated_cell_area = []
                calculated_cell_center_r = []
                calculated_cell_centroid_r = []
                for k in range(len(subset.element)):
                    cell_id = ids_to_plot.grid_ggd[0].grid_subset[subset_id].element[k].object[0].index
                    cell_indices.append(faces[cell_id-1]-1)
                    cell_area.append(face_area[cell_id-1])
                    calculated_cell_area.append(calculated_face_area[cell_id-1])
                    calculated_cell_center_r.append(calculated_face_center_r[cell_id-1])
                    calculated_cell_centroid_r.append(calculated_centroid_r[cell_id-1])
        
                # add area of cell to vtk
                cell_data["ids_face_area"] = cell_area
                cell_data["calculated_face_area"] = calculated_cell_area
                cell_data["calculated_face_center_r"] = calculated_cell_center_r
                cell_data["calculated_cell_centroid_r"] = calculated_cell_centroid_r
                all_powers = np.zeros(len(calculated_cell_centroid_r))

            elif dimension == 2:
                for k in range(len(subset.element)):
                    cell_id = ids_to_plot.grid_ggd[0].grid_subset[subset_id].element[k].object[0].index
                    cell_indices.append(edges[cell_id-1]-1)

            else:
                for k in range(len(subset.element)):
                    cell_id = ids_to_plot.grid_ggd[0].grid_subset[subset_id].element[k].object[0].index
                    cell_indices.append(pts[cell_id-1])

    
            for process_ind in range(len(ids_to_plot.process)):
                ion_array_length = len(ids_to_plot.process[process_ind].ggd[0].ion)
                neutral_array_length = len(ids_to_plot.process[process_ind].ggd[0].neutral)
                # iterate over ion arrays
                for ion_ind in range(len(ids_to_plot.process[process_ind].ggd[0].ion)):
                    #print("Process", process_ind, "ion", ion_ind)
                    if len(ids_to_plot.process[process_ind].ggd[0].ion[ion_ind].emissivity) == grid_subset_size:
                        label = "process"+str(process_ind)+"_ion"+str(ion_ind)+"_emissivity_"+ids_to_plot.process[process_ind].ggd[0].ion[ion_ind].label
                        values = ids_to_plot.process[process_ind].ggd[0].ion[ion_ind].emissivity[i].values
                        if dimension == 1:
                            point_data[label] = values
                        elif dimension == 2:
                            cell_data[label] = values
                        elif dimension == 3:
                            all_powers += np.array(values)
                            total_power = np.sum(np.array(values)*calculated_cell_area*2*np.pi*calculated_cell_center_r/1e6)
                            total_power_centroid = np.sum(np.array(values)*calculated_cell_area*2*np.pi*calculated_cell_centroid_r/1e6)
                            cell_data[label] = values
                            

                    if len(ids_to_plot.process[process_ind].ggd[0].ion[ion_ind].state) > 0:
                        for state in ids_to_plot.process[process_ind].ggd[0].ion[ion_ind].state:
                            state_label = "process"+str(process_ind)+"_ion"+str(ion_ind)+"_STATElabel_"+state.label
                            if len(state.emissivity) > 0:
                                values = state.emissivity[i].values
                                if len(values) > 0:
                                    if dimension == 1:
                                        point_data[state_label] = values
                                    elif dimension == 2:
                                        cell_data[state_label] = values
                                    elif dimension == 3:
                                        total_power = np.sum(np.array(values)*calculated_cell_area*2*np.pi*calculated_cell_center_r/1e6)
                                        total_power_centroid = np.sum(np.array(values)*calculated_cell_area*2*np.pi*calculated_cell_centroid_r/1e6)
                                        cell_data[state_label] = values

                # iterate over neutral arrays
                for neutral_ind in range(len(ids_to_plot.process[process_ind].ggd[0].neutral)):
                    #print("Process", process_ind, "neutral", neutral_ind)
                    if len(ids_to_plot.process[process_ind].ggd[0].neutral[neutral_ind].emissivity) == grid_subset_size:
                        label = "process"+str(process_ind)+\
                            "_neutral"+str(neutral_ind)+\
                            "_emissivity_"+ids_to_plot.process[process_ind].ggd[0].neutral[neutral_ind].label
                        values = ids_to_plot.process[process_ind].ggd[0].neutral[neutral_ind].emissivity[i].values
                        if dimension == 1:
                            point_data[label] = values
                        elif dimension == 2:
                            cell_data[label] = values
                        elif dimension == 3:
                            all_powers += np.array(values)
                            total_power = np.sum(np.array(values)*calculated_cell_area*2*np.pi*calculated_cell_center_r/1e6)
                            total_power_centroid = np.sum(np.array(values)*calculated_cell_area*2*np.pi*calculated_cell_centroid_r/1e6)
                            
                            cell_data[label] = values


                    if len(ids_to_plot.process[process_ind].ggd[0].neutral[neutral_ind].state) > 0:
                        for state in ids_to_plot.process[process_ind].ggd[0].neutral[neutral_ind].state:
                            state_label = "process"+str(process_ind)+"_neutral"+str(neutral_ind)+"_STATElabel_"+state.label
                            values = state.emissivity[i].values
                            if len(values) > 0:
                                if dimension == 1:
                                    point_data[state_label] = values
                                elif dimension == 2:
                                    cell_data[state_label] = values
                                elif dimension == 3:
                                    total_power = np.sum(np.array(values)*calculated_cell_area*2*np.pi*calculated_cell_center_r/1e6)
                                    total_power_centroid = np.sum(np.array(values)*calculated_cell_area*2*np.pi*calculated_cell_centroid_r/1e6)
                                    cell_data[state_label] = values
            
            if dimension == 3:
                cell_data["Q[$W/m^3$]"] = all_powers
                cell_data["cells"] = cell_indices
                cell_data["nodes"] = pts
                generate_faces_vtk(os.path.join(ggd_folder, shot_run+"_"+name+"_radiation_dimension"+str(dimension)+".vtk"), cell_data)

            elif dimension == 2:
                cell_data["cells"] = cell_indices
                cell_data["nodes"] = pts
                generate_edges_vtk(os.path.join(ggd_folder, shot_run+"_"+name+"_radiation_dimension"+str(dimension)+".vtk"), cell_data)


