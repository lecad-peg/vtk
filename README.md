# README #

Various Python scripts for VTK mesh processing with JINTRAC, SOLPS-ITER, radiation.

 - [add_radiation_core_edge_v2.py](python/add_radiation_core_edge_v2.py)
 - [create_core_mesh.py](python/create_core_mesh.py)		
 - [merge_SOLPS_and_JINTRAC.py](python/merge_SOLPS_and_JINTRAC.py)
 - [create_SOLPS_all_ggd_vtks.py](python/create_SOLPS_all_ggd_vtks.py)	
 - [helper_functions.py](python/helper_functions.py)	
 - [vtkUtilities.py](python/vtkUtilities.py)

### Instructions ###

* Run *add_radiation_core_edge_v2.py* on ITER cluster. It produces a VTK file of structured grid, where the radiation is defined on each rectangular cell. Definitions for shot and run are at the moment hardcoded at the bottom of the script. Different shots and runs can be defined there for other JINTRAC scenarios. This script will output a VTK file *JINTRAC_radiation_shot134000_run60.vtk* which can be visualized in Paraview (see below).

  ![Scheme](images/jintrac_radiation.png)

* Run *create_SOLPS_all_ggd_vtks.py*. This script will create a new directory *all_ggd_vtks*. It will then iterate over the radiation IDS for shots that are currently hardcoded in the script (namely 123275-79) and save all ggd structures into this new directory. Inside are VTK files for all ggd structures, such as separatrix, divertor inner outer legs, etc. In file names, dimension1,2,3 correspond to the type of element i.e. 1 - node, 2 - edge, 3 - cell (triangles). This can be easily visualized in Paraview.

* Run create_core_mesh.py. It takes two inputs:
    * *JINTRAC_radiation_shot134000_run60.vtk*.
    * *shot123275_run1_CORE_BOUNDARY_radiation_dimension2.vtk*. This file contains a SOLPS contour of core boundary. The JINTRAC profiles are patched within this contour. The contour is seen below, along with the JINTRAC profile.

  ![Scheme](images/jintrac_radiation_solps_inner_contour.png)

  In the script JINTRAC rectangular cells are iterated over and a check is done whether cell nodes are inside the contour. If all cell nodes are inside the contour, the rectangular cell is converted into two triangular cells. If no nodes are inside the contour, the cell is immediately eliminated and program continues. Then there are three other options: 1, 2 or 3 nodes inside a contour. For these options intersections are then calculated between contour and the cell edges and generate triangles appropriately. The new file is *JINTRAC_core_mesh_s134000_r60.vtk*. See below.

  ![Scheme](images/jintrac_core.png)

* The final step is to merge this core mesh with SOLPS solution. Run the *merge_SOLPS_and_JINTRAC.py*. This script will take both JINTRAC core and SOLPS solution and merge them together. At the same time I remove the orphan nodes (nodes which do not belong to any cell -> if you check JINTRAC core solution from previous step you will notice that the nodes from JINTRAC rectangular grid are still present in the JINTRAC core solution) and reorder the cells. The final solution is then below

  ![Scheme](images/jintrac_solps_final.png)
